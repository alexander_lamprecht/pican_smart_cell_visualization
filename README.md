# Raspberry Pi based Smart Cell Visualization

This repository contains the source code and the documentation of the Raspberry Pi based smart cell visualization.

## Content of This Repository

Folder | Content
------ | ------
[`config`](config)   | This folder contains some config files, which have been edited on the Raspberry Pi during set up.
[`images`](images) | This folder contains images.
[`javascript`](javascript) | This folder contains the JavaScript source code (incl. socket server, web server and client web interface).
[`misc`](misc) | This folder contains different files like datasheets or CSV files.
[`python`](python) | This folder contains the Python source code.
[`scripts`](scripts) | This folder contains some scripts, which ease the use of the application.

## System Overview

### Actual System

![](images/overview.JPG "System Architecture")

### System Architecture

![](images/SystemArchitecture-V03.png "System Architecture")

### Hardware Architecture

![](images/hwarchitecture.jpg "Hardware Architecture")

### Software Architecture

![](images/swarchitecture.jpg "Software Architecture")


## Getting Started

This section contains the steps to get the system up and running from scratch. A basic knowlegde about Linux and the Raspberry Pi is assumed. The following topics are described:

1. Enabling SSH
2. Accessing the Raspberry Pi via SSH in the Terminal
3. Accessing the Raspberry Pi via VNC
4. Wireless Hotspot Setup
5. Python Setup
6. JavaScript Setup
7. Testing Python-JavaScript Communication

### 1. Enabling SSH

In some Linux distributions SSH is enabled by default. In the Raspbian version used for this project (Raspbian Jessie) SSH was disabled by default. After cloning the Raspbian image to a SD card, there are two partitions (one called `boot` and one named with a random pattern like `62ca0b6d-6291-4c40-b1fd-11bc291e4a38`. To enable SSH, simply create an empty file named `ssh` on the `boot` partition. When booting, the green ACT LED needs to blink in a random fashion. This LED shows that the Raspberry Pi is able to read from the SD card.

### 2. Accessing the Raspberry Pi via SSH in the Terminal

The Ethernet interface of the Raspberry Pi is enabled by default. So either you connect the Raspberry Pi to a router (or a switch) or you connect it directly to your computer and configure your computer's Ethernet interface as "*shared to other computers*" (this is an option of the network manager in Debian/Ubuntu). Then you need to get to now the IP address of the Raspberry Pi. Under Linux this can be done by using the tool `nmap` with the following command:

```
nmap -sn <IP>/<SUBNETMASK>
```

 where `<IP>` is the IP adress of the network and `<SUBNETMASK>` the mask for the subnetwork. After having found the IP address of the Raspberry Pi, you simply can connect to it via SSH with the following command:
```
ssh -X pi@raspberrypi.local
```

where the `-X` option enables the forwarding of the output of the X-server (so also desktop-based application can be accessed from the desktop computer) and instead of `raspberrypi.local` also the IP address of the Raspberry Pi can be used. The credentials of the Raspberry Pi have not been changed and are still:

* Username: `pi`
* Password: `raspberry`

### 3. Accessing the Raspberry Pi via VNC

When using Raspbian 8 (Jessie), a VNC server is already preinstalled. So you only need to start a session by executing the script `startVNCserver.sh`(see [here](scripts/startVNCserver.sh)) and connect to this session from another computer with the following command (from a Linux host), where `<PORT>` needs to be replaced by the port on which the started session of the VNC server is available:

```
vncviewer raspberrypi.local:<PORT>
```

Therefore you need to install a VNC viewer software (preferrably from RealVNC, see [here](https://www.realvnc.com/en/connect/download/viewer)).

### 4. Wireless Hotspot Setup

To be able to access the Raspberry Pi in headless mode without connecting an Ethernet cable to it, a wireless hotspot using the Wifi interface of the Raspberry Pi can be set up. Therefore first the packages `hostapd` and `dnsmasq` need to be installed (with `apt-get`). Then some configuration files have to be edited. For details have a look at this [webpage](http://www.raspberryconnect.com/network/item/320-rpi3-auto-wifi-hotspot-if-no-internet-oldscript). In the folder [`config`](config) all files which have been edited and their respective path can be found. If the Raspberry Pi needs Internet access at some point, the hotspot needs to be disabled. Therefore the script `stopHotSpot.sh` (see [here](scripts/stopHotSpot.sh)) has been written. To enable the wireless hotspot without having to restart the Raspberry Pi, just run the script `startHotSpot.sh`(see [here](scripts/startHotSpot.sh)) as root.

### 5. Python Setup

**IMPORTANT:** In this project Python 2.7 is used!

#### a) Setting up a Virtual Environment and Install Required Packages

Since Python is already installed on the Raspberry Pi, only the necessary packages need to be installed. To restrict the installation of these packages only to this project, a virtual environment has been set up. Therefore the program `virtualenv` is used (alternative: `miniconda`). It can be installed with `apt-get install virtualenv`. To set up a new virtual environment just run the following commands in the terminal of the Raspberry Pi (the lines beginning with a "#" are only comments):

```
VIRTUAL_ENV=$HOME/.virtualenv

# Prepare isolated environment
virtualenv $VIRTUAL_ENV

# Activate isolated environment
source $VIRTUAL_ENV/bin/activate
```

Setting up the environment variable makes it possible to activate the virutal environment from any path. To permanently add the environment variable, just add the following line to the file `~/.bashrc` and it gets set every time a terminal is started:

```
export VIRTUAL_ENV="$HOME/.virtualenv"
```

As soon as the virtual environment is activated, the binaries both for `python` and the package manager `pip` from the path `~/.virtualenv/bin/` are used, when typing the respective command in the terminal. Now the necessary Python packages can be installed. At this point the following packages have been installed with the following commands:

```
pip install -U socketIO-client==0.7.2
pip install -U enum==0.4.6
```

The package `socketIO-Client` enables the communication via WebSocket. Since there have been some issues with the version, the suffix `==0.7.2` ensures that the correct version is installed. An alternative version is `0.5.7.2`. For details see [here](https://pypi.python.org/pypi/socketIO-client)). The package `enum` is used to denote the message type (in the file `myMessage.py`) of the specific CAN protocol used in the communication between the cell controllers.

#### b) Setting up the PICAN2 Add-on Board

For CAN communication with the smart cell demonstrator the add-on board PICAN2 from *SK Pang electronics* is used (for details see [here](http://skpang.co.uk/catalog/pican2-canbus-board-for-raspberry-pi-23-p-1475.html). It is plugged onto the 40 pin extension header of the Raspberry Pi and communicates via SPI with the Raspberry Pi. To test the PICAN2 add-on board without affecting the smart cell demonstrator the Peak CAN USB adapter is used. Therefore first the respective driver and control software has to be installed on a desktop computer (for downloads see [here](http://www.peak-system.com/PCAN-USB.199.0.html)). In the scope of this project a Windows computer was used to send and receive CAN frames with the Peak CAN USB adapter. Here the software "*PCAN-View*" was installed and provides an simple user interface. After having installed the software, just connect the signals `CAN_H`, `CAN_L` and `GND` of the PICAN2 add-on board to the Peak CAN USB adapter and use the Python scripts [`RX-test.py`](python/can-test/RX-test.py) and [`TX-test.py`](python/can-test/TX-test.py) to send/receive CAN frames to/from the Peak CAN USB adapter. Each time you want to use the CAN interface on the Raspberry Pi, you need to bring it up first. Therefore just execute the script [`startCAN.sh`](scripts/startCAN.sh) and the script [`stopCAN.sh`](scripts/stopCAN.sh) to bring it down again (if needed). Don't forget to set the jumper `JP3` to enable the bus termination of 120 Ohms. In the following image the set up with the mounted and connected PICAN2 add-on board and the Raspberry Pi can be seen:

![](images/pi.jpg "Raspberry Pi 3 with PICAN2 add-on board")

First some prerequisites need to be ensured in order to enable the communication between PICAN2 and Raspberry Pi. The manufacturer provides some instructions for this in the [user guide](misc/PICAN2_UserGuide_RefB_V1.2.pdf) on the product page in Section 3 "*Software installation*". These instructions tell you first to update and upgrade the Raspberry Pi (with `apt-get`) and then to edit the file `config.txt` located in the folder `/etc/` (see [here](config/config.txt)). Just add these three lines at the end of the file and shutdown the Raspberry Pi:

```
dtparam=spi=on
dtoverlay=mcp2515-can0,oscillator=16000000,interrupt=25
dtoverlay=spi-bcm2835-overlay
```

After mounting the PICAN2 and booting the Raspberry Pi, you now can bring up the CAN interface with the following command (as root). If there is no error message, the device was brought up successfully.

```
/sbin/ip link set can0 up type can bitrate 1000000
```

#### c) Test Menu Application

To ease the process of initializing all interfaces and to test different functionalities on their own, the test application [`menu.py`](python/menu.py) was written. This application is designed as a menu, where the user can choose between different options. It also shows, if there is a connection to the socket server by printing the socket server address and the respective port. Additionally you can see if the CAN interface is already brought up. In the image below you can see the output, after starting the application by typing the command `./menu.py` into the terminal.

![](images/testprogram.png "Test menu application")

The first line of the Python script ensures that the correct Python binary is used to execute the script:

```
#!/home/pi/.virtualenv/bin/python
```

Since the development of the menu application was based on implementing every single functionality on its own, there are several options which don't have to be used in the actual deployment of the whole visualization. That's why it can be seen as a debug interface for testing all single function blocks on their own. Only option `[a]` (forwarding the cell status to the socket server) is used in the final application by executing the menu application with the option `-f` (or `--forward`) like this:

```
./menu.py -f
```

When this option is used, the output to the terminal is completely suppressed, so that it can be executed within another shell script.

### 6. JavaScript Setup

For visualizing the cell status in a graphical way with the Raspberry Pi different possibilities were considered. One of them was a [Qt](https://www.qt.io/)-based alternative since there already exists an application based on Python and Qt. This application can be found the folder `can_record_analysis` of this [repository](https://bitbucket.org/snarayanaswami/git-can-analyzer-alex-and-ananth). For the following reasons it has been decided to develop the Raspberry Pi based visualization in a JavaScript environment and not based on Qt:

* The Qt based application is set up in an Eclipse project under Windows (including the use of Windows \*.dll drivers) and thus the porting to the Raspberry Pi is expected to be not as straight forward as desired.
* The possibility of accessing the visualization remotely from another computer is an attractive feature and already suggests the use of a browser based visualization where JavaScript is the most obvious and sensible alternative.
* JavaScript is considered to be an easy to use and quickly to start with programming language and requires on the client side simply a JavaScript enabled web browser and only a few basic libraries on the server side. This way the footprint of the whole application can be minimized.
* The client-side JavaScript library [p5.js](www.p5js.org) provides a simple and easy to use framework for building graphical and interacting web interfaces. There is a comprehensive [video tutorial](https://www.youtube.com/user/shiffman/playlists?sort=dd&view=50&shelf_id=14), where all the basic concepts as well as advanced features are explained in detail.

#### a) Installation of Node

[Node.js](https://nodejs.org) is an open-source, cross-platform JavaScript run-time environment for executing JavaScript code server-side ([source](https://en.wikipedia.org/wiki/Node.js)). In the case of this project the socket and HTTP server is executed with node.js and runs all the time the web interface needs to be accessed. To install `node` and `npm` (the node package manager) just follow the following instructions (see [here](https://github.com/creationix/nvm) for the source code and [here](https://stackoverflow.com/questions/39981828/installing-nodejs-and-npm-on-linux/39981888#39981888) for installation details). First download and execute an installation script by executing the following command in a terminal on the Raspberry Pi:

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

This clones the source code of `nvm` into `~/.nvm` and adds a few necessary commands to the `~/.bashrc` file. To test that `nvm` was properly installed, close and re-open the terminal and enter `nvm`, which should output the help text of `nvm`. If so, you can now install node.js by typing `nvm install <version>` (version used in this project: `8.4.0`). In order to access `node` and `npm` as sudo (in order to have lower than 1024 ports, in this project port 80, default for HTTP, is used) you should run the following commands:

```
n=$(which node)
n=${n%/bin/node}
chmod -R 755 $n/bin/*
sudo cp -r $n/{bin,lib,share} /usr/local
```

After having done this a new configuration file can be set up by typing the command `npm init` into the terminal while being in the respective directory you want the server to run in. After hitting ENTER you have to enter a few details, where basically only the option "*entry point*" is important to be set as the file name of the server JavaScript file (in this project `server.js`). When the set up is finished the folder `node_modules` and the files `package.json` and `package-lock.json` are created.

#### b) HTTP Server

Since we want to access the web interface via the network it is necessary to run a HTTP server on the Raspberry Pi. Therefore the node package `express` (version `4.15.4` in this project) has to be installed. Just type in the following command to do so:

```
npm install express --save
```

#### c) Socket Server

For the communication between the Python and JavaScript applications the full duplex capable communication protocol called WebSocket is used. This protocol is designed to be used in realtime web applications and server-client based scenarios. One implementation is [socket.io](https://socket.io/) which comes with a client side and a server side JavaScript library. To install it for the server side JavaScript application just run the following command.

```
npm install socket.io@1.7.2 --save
```

**IMPORTANT:** Version `2.0.3` of `socket.io` raises errors when running the socket server!

#### d) Web Interface Configuration

After having installed the packages `express` and `socket.io`, the file `package.json` should have two additional dependencies showing that the packages have been installed successfully. Also with the command `npm list` you can check which packages are installed and which versions are used. The directory containing the JavaScript source code has the following structure for this project:

* `node_modules`: contains many subfolders with the module source code (77 in total in this project)
* `public`: contains content to be accessible via the browser
	* `graphical`: contains the graphical visualization
	* `rawmessages`: contains the raw message view
	* `systemarchitecture`: contains a visualization of the system architecture
	* `libraries`: contains own (with "my" prefix) and public JavaScript libraries
	* `src`: contains images and CSV file to be used for the web page
	* `index.html`: represents the root index file with the option to choose which site to view
* `package.json`: contains information about the packages
* `package-lock.json`: contains information about the packages
* `server.js`: contains the server code

The folders `graphical`, `rawmessages` and `systemarchitecture` each contain an `index.html` and a `sketch.js` file to display the respective content. In the source directory of the `public` folder there is also an `index.html` file with the option to choose which site to display. The file `server.js` is not visible to the client and has to be run by the node.js JavaScript runtime environment to start the HTTP and socket server. In the folder `libraries` you can find both self written libraries (with "my" prefix" and public libraries. A common way when using JavaScript libraries is to refer to a URL instead of a local file, but this has been avoided, since the web interface should also work without an Internet connection. The file `socket.io.js` has been downloaded from [here](https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js), `graphica(.min).js` has been downloaded from [here](https://github.com/jagracar/grafica.js) and `dat.gui.js` from [here](https://github.com/dataarts/dat.gui). All libraries with "p5" as prefix are referenced on the p5.js [homepage](https://p5js.org/) in the section "*libraries*".

For a detailed description on how to set up the server and client scripts you can have a look at the Youtube tutorial of "*The Coding Train*" chapter 12.1 to 12.4 (see [here](https://www.youtube.com/watch?v=bjULmG8fqc8&list=PLRqwX-V7Uu6b36TzJidYfIYwTFEq3K5qH)).

### 7. Testing Python-JavaScript Communication

To test that the communication between the JavaScript based web interface and the Python script is working, the so called "*Raw Messages View*" can be used to receive and send strings via WebSocket on the JavaScript side. For the Python side the first three menu entries of the test menu application `menu.py` give a possibility to send and receive simple strings to the socket server.

As soon as the basic communication between the Python and JavaScript software module are ensured the forwarding of the cell status messages can be tested, first with the "*Raw Message View*" and then with the "*Graphical Visualization*" of the web interface. Important here is that the CAN interface needs to be brought up first and the smart cell demonstrator of course needs to be running. The forwarding service detects CAN messages which contain a voltage value and only forwards the cell ID and the respective voltage value in a list. In the "*Raw Messages View*" these messages get printed as soon as the check box "Listen and print incoming messages" is enabled.

In the following the most important CAN message types, as implemented in the used version of the smart cell demonstrator, are listed.

**IMPORTANT: This table is based on information extracted from the source code of the smart cell demonstrator and not on any official documentation!**

| Message Type | Hex Representation | Target ID | Origin ID | Payload |
|:---|:---|:---|:---|:---|
| `MSG_TYPE_UNBLOCK`         | `0x0001` | `0xFF`         | `[selfID]` | `[senderID]`, `[receiverID]` |
| `MSG_TYPE_BLOCK`           | `0x0002` | `0xFF`         | `[selfID]` | `[senderID]`, `[receiverID]` |
| `MSG_TYPE_STATUS_RESPONSE` | `0x0003` | `[targetID]`   | `[selfID]` | `[blockerID0]`, `[blockerID1]` |
| `MSG_TYPE_VOLTAGE`         | `0x0011` | `0xFF`         | `[selfID]` | `[Vol0]`, `[Vol1]`, `[Vol2]`, `[Vol3]` (32 bit single precision float with voltage value) |
| `MSG_TYPE_SEND_REQ`        | `0x0020` | `[senderID]`   | `[selfID]` | empty |
| `MSG_TYPE_SEND_ACK`        | `0x0021` | `[receiverID]` | `[selfID]` | empty |
| `MSG_TYPE_SET_BALANCING`   | `0x0030` | `[targetID]`   | `[selfID]` | `[balancingEnabled]` |
| `MSG_TYPE_SET_STRATEGY`    | `0x0031` | `[targetID]`   | `[selfID]` | `[balancingStrategy]` |


## Deployment of the Application

To ease the deployment of the whole visualization application, the shell script `systemControl.sh` was written. Through this script, which can be executed (as root!) either directly on a terminal on the Raspberry Pi or via a SSH connection, the single services can be started, stopped and the autostart can be enabled and disabled. The following three points have to be carried out in order to run the visualization:

* The CAN interface has to be brought up.
* The socket/HTTP server has to be started.
* The forwarding of voltage messages from the CAN bus to the socket server has to be started.

These are covered by the menu entries `[1]`, `[2]` and `[3]`. It is crucial to start the forwarding of the CAN messages to the socket server last, since it requires a brought up CAN interface and an available socket server. Additionally, the VNC server and the wireless hotspot can be controlled through the script via the menu entries `[4]` and `[5]`.

![](images/systemcontrol.png "System control application")

In order to enable the execution of the script from the home directory of the Raspberry Pi, a symbolic link (with the same name) can be created with the following command after changing to the directory where the script is stored:

```
ln -s systemControl.sh ~/systemControl.sh
```


## Known issues / difficulties

* During the balancing, the involved cells (the ones which are exchanging charge) only very rarely send voltage updates (every few seconds instead of several times per second). This way, the visualization for these cells is not real-time anymore.
* Since the main JavaScript visualization process is implemented by a drawing routine which is executed in an endless loop, the execution of on the Raspberry Pi needs a rather high amount of CPU resources (monitored by the command line tool `htop`). This sometimes results in a not responding web browser which needs to be restarted in order to work properly again. As first measure to handle this issue, the swap size of the Raspberry Pi has been increased from the default 100 MB to 2 GB (see [here](https://www.bitpi.co/2015/02/11/how-to-change-raspberry-pis-swapfile-size-on-rasbian/) for details how to do that) and it is suggested to adjust the repeating frequency of the drawing routine or to introduce a mechanism so that the visualization is only redrawn if something changes.
* The current derivation of the state of charge is rather inaccurate, since it is implemented as a simple look up table with a mapping between voltage value and state of charge value. This way, the state of charge is fluctuating strongly if the voltage is dropping (e.g. when a load is connected), which is of course not realistic.
* As the scaling of the visualization is only dependent on the window size and the battery symbols are arranged horizontally, the view of the web interface on mobile devices in portrait mode is not~optimal.
* If you want to access the web interface from a Windows computer or a Android phone, the domain name `raspberrypi.local` does not work. In order to access the web interface from one of these device types, the IP address of the Raspberry Pi has to be used. Currently the connection by using the domain name has been successfully tested on different Linux computers and on an Apple Iphone 6s.
* The auto boot functionality implemented in the script `systemControl.sh` using the Linux native utility `systemd` is not working properly and needs to be tested and revised more intensely. Sometimes a service (e.g. the socket/HTTP server) is not started at all or ends after a certain while. One alternative for starting bash scripts at boot is to add the respective commands to start a script in the file `.bashrc`, located in the home directory of the Raspberry Pi. The commands placed in this configuration file are executed every time the bash starts, which is basically every time a terminal is opened (e.g. when connecting via SSH to the Raspberry Pi). Another alternative is the file `rc.local` located in the folder `/etc/init.d/`, a shell script which is executed at every boot of the Raspberry Pi.
* When the auto boot of the wireless hotspot is disabled and a you want to connect the Raspberry Pi to a wireless network by using the desktop network manager, the wireless interface needs to be reset by executing the `stopHotSpot.sh` script. The connection to the new lab Wifi "*ESTL-LAB*" is a reasonable alternative to an Ethernet connection.
* The voltage plot which can be found below each battery symbol is currently initialized with zeros and therefore the automatic scaling of the plot's y-axis only works as soon as these zeros are replaced by actual values. It is suggested to increase the array length of the plot's x-axis dynamically during startup of the web interface.
* When no more status updates are received from a specific cell within a predefined timeout, the respective battery symbol is printed gray, but the voltage plot assigned to this cell just stays as it was when the last value was received. This should be handled nicer, e.g. in deleting the plot content completely as soon as the cell is not up-to-date anymore.

## Future work

* Currently, there is no animation of active cell balancing. It should at least be visualized if balancing is active and optimally which cells are involved in exchanging charge and which cells are blocking.
* Future battery management systems are supposed to have much more cells than the demonstrators. To provide the compatibility to such systems, the support for displaying a much bigger number of cells (more than 40) should be added. Therefore a algorithm needs to be defined which aligns the cells in multiple rows and adjusts the size according to the number.
* Currently the balancing functionality has to be started via a dedicated HW-button located on the board below the very left cell. In future it would be convenient to start the balancing via a button on the web interface. Also the possibility to stop the balancing could be introduced (currently not implemented at all). It has been experimented to send the exact same CAN message which was noticed when pressing the HW-button for starting balancing, but this did not result in a balancing activity.
* In the former analyzing software a view with the voltage plots of all cells in one single diagram over a longer period of time is available. This way, the voltage (or state of charge) convergence can be seen nicely. Such a view is suggested for the web interface as well.
* The number of cells is presently a fixed value which is set up in one of the JavaScript files. Since at start up of the smart cell demonstrator there is a phase where the cells negotiate their IDs, this exchanged information could be monitored and used to define the number of cells dynamically during start up of the smart cell demonstrator.
* It is suggested to introduce an activity indicator (comparable with the activity LED common RJ45 jacks are equipped with) which shows the activity of received CAN messages.
* To ensure a certain level of security, a login screen for the web interface could be introduced. Especially if the web interface is made accessible from the Internet. If done so, the usage of HTTPS instead of HTTP for the socket/web server is highly recommended.
* To also introduce some analyzing functionalities, the recording of the CAN messages, e.g. into a CSV file could be implemented.
* If the web interface is planned to be accessed rather from the Raspberry Pi directly than from the remote web clients, it is suggested to enable the auto boot of a browser in fullscreen mode, so that the web interface is shown immediately after the start up.

## Author

**Hannes Bohnengel** - *Master student at Technical University of Munich* - [hannes.bohnengel@tum.de](mailto:hannes.bohnengel@tum.de)
