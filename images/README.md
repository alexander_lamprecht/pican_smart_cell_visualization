# What is in here?

This folder contains some images used in the main [`README.md`](../README.md). The \*.xml files can be edited with the browser based tool [draw.io](www.draw.io) and exported to different formats like \*.png, \*.jpg or \*.pdf.
