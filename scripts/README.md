# What is in here?

This folder contains some shell scripts to control to control different modules used for this project.

Folder/File | Description
------ | ------
[`init.d`](init.d) | This folder contains scripts which have to be placed into the Linux folder `/etc/init.d/` in order to be executed at system startup.
[`systemControl.sh`](systemControl.sh) | This shell script provides a centralized user interface to execute all other shell scripts and to enable/disable the automatic startup of the used software modules.  
[`startCAN.sh`](startCAN.sh) | Brings up the CAN interface with a specified bitrate.
[`stopCAN.sh`](stopCAN.sh) | Brings down the CAN interface.
[`startHotSpot.sh`](startHotSpot.sh) | Starts a wireless hotspot by using the Linux software packages `hostapd` and `dnsmasq`.
[`stopHotSpot.sh`](stopHotSpot.sh) | Stops the wireless hotspot.
[`startVNCserver.sh`](startVNCserver.sh) | Starts a VNC server with a specified port by using the VNC server from RealVNC which is preinstalled on the Raspberry Pi (see [here](https://www.realvnc.com/en/connect/download/vnc/raspberrypi/)).
[`stopVNCserver.sh`](stopVNCserver.sh) | Stops the VNC server.
