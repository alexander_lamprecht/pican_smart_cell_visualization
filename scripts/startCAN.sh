#!/bin/bash

######################################################################################
#
# Filename:        startCAN.sh
# Author:          Hannes Bohnengel
# Last modified:   26 Sep 2017
#
# Comments:
# -------------
# This is a bash script to bring up the CAN interface available on the PICAN2 add-on
# board after having set up the /boot/config.txt file.
#
# Execute like this (as root):
# $ ./startCAN.sh 1000000
#
######################################################################################

# Get bitrate from first argument
if [ -z "$1" ]
  then
    BITRATE=1000000
    echo "No argument supplied. Setting bitrate to 1000 kbit/s."
else
  BITRATE=$1
fi

# Define command
CMD="ip link set can0 up type can bitrate $BITRATE"

# Output:
echo "Bringing up the CAN interface on can0 with the bitrate $BITRATE"
$CMD
