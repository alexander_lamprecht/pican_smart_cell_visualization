# What is in here?

This folder contains the startup scripts for which a link needs to be placed inside `/etc/init.d/` so that they will be recognized by the Linux system program `systemd`

File | Description
------ | ------
[myCAN](myCAN)          | Short description
[mySocket](mySocket)    | Short description
[myForward](myForward)  | Short description
[myHotSpot](myHotSpot)  | Short description

## How create a start up script

In the directory `/etc/init.d/` you can find all scripts which are exectued at startup. The file `skeleton` is a template for new scripts and has also been used for this project. When a script is enabled for execution at startup a link with the prefix "S01" is created in `/etc/rcX.d/` (where the "X" is a number between 0 and 6 and denotes the runlevel). "S" stands for start and the two digit number following defines the order of execution. To enable the exectution at startup of a certain script, which already exists in `/etc/init.d/` just run the following command as root (`[script-name]` is replaced by the name of the script):
```
systemctl enable [script-name]
```
When you want to disable the startup exectution replace the `enable` by `disable` and each "S" in the prefix of the links in the `/etc/rcX.d` folders will be replaced by a "K" (kill).

To mark the script as executable, the follwoing command has to be executed as root while being in the directory where the script is stored:
```
chmod 755 [name-of-script]
```
The number 755 denotes, that only the root can write changes to the script but all others can read and execute it (for details about linux access rights see [here](https://www.linux.com/learn/understanding-linux-file-permissions) or [here](https://www.linux.org/threads/file-permissions-chmod.4124/)).

## How to create a link in the terminal

Execute the following command in the terminal. It needs root rights, since the directory /etc/ can only be written to or changed as root.

```
sudo ln [path-to-file]/[file-name] /etc/init.d/[link-name] 
```

In our case `[path-to-file]` is `/home/pi/CAN-Analyzer/pi-can-analyzer/scripts/init.d`, the file name is one of the above listed files and the link name is exactly the same as the file name.
