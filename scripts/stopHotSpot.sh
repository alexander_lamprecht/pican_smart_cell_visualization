#!/bin/bash

######################################################################################
#
# Filename:        stopHotSpot.sh
# Author:          Hannes Bohnengel
# Last modified:   26 Sep 2017
#
# Comments:
# -------------
# This is a bash script to stop a running Wifi hotspot (using hostapd and dnsmasq)
# and needs to be executed as root
#
######################################################################################

# Output:
echo "Closing wireless hotspot"
ifconfig wlan0 down
echo "Stopping dnsmasq and hostapd..."
service dnsmasq stop
service hostapd stop
echo "Restarting the DHCP client..."
wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf > /dev/null 2>&1
ifconfig wlan0 down
service dhcpcd restart
ifconfig wlan0 up
echo "Hotspot closed"
