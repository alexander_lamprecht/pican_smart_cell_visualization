#!/bin/bash

######################################################################################
#
# Filename:        startHotSpot.sh
# Author:          Hannes Bohnengel
# Last modified:   11 Oct 2017
#
# Comments:
# -------------
# This is a bash script to start a Wifi hotspot (using hostapd and dnsmasq)
# and needs to be executed as root
#
######################################################################################

# Set up IP address of subnetwork
IP="192.168.0.1"

# Set up netmask of subnetwork
NETMASK="255.255.255.0"

# Output: 
echo "Starting a wireless hotspot"
echo "Setting up the wlan0 interface..."
ifconfig wlan0 down
ifconfig wlan0 $IP netmask $NETMASK up
echo "Starting dnsmasq and hostapd..."
service dnsmasq start
service hostapd start
echo "Hotspot started"
