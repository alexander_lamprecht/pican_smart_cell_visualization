#!/bin/bash

#################################################################
#
# File name:     systemControl.sh
# Author:        Hannes Bohnengel
# Last modified: 26 Sep 2017
# Revision:      0.1
#
# This script provides a easy menu application to control 
# the system components
# 
# !!! IMPORTANT: This script has to be executed as root !!!
#
# !!! All changes which are done here (e.g. changing paths) have
# !!! to be done also in the respective startup scripts
# !!! See /etc/init.d/my[CAN/Socket/Forward] 
#
#
#################################################################

# Set up CAN bitrate
BITRATE=1000000

# Path to socket/HTTP server file
PATH_TO_SERVER_JS="/home/pi/CAN-Analyzer/pi-can-analyzer/javascript/webHandler"

# File name of socket/HTTP server file
SERVER_JS="server.js"

# Path to python application file
PATH_TO_MENU_PY="/home/pi/CAN-Analyzer/pi-can-analyzer/python"

# File name of the python main application
MENU_PY="menu.py"

# Path to shell scripts
PATH_TO_SH_SCRIPTS="/home/pi/CAN-Analyzer/pi-can-analyzer/scripts"

# Name of script to start the wireless hotspot
START_HOTSPOT_SH="startHotSpot.sh"

# Name of script to stop the wireless hotspot
STOP_HOTSPOT_SH="stopHotSpot.sh"

# Set up the different commands to be exectued
CAN_UP="ip link set can0 up type can bitrate $BITRATE"
CAN_DOWN="ip link set can0 down"
SOCKET_SERVER_START="node $PATH_TO_SERVER_JS/$SERVER_JS"
SOCKET_SERVER_STOP="killall node"
#FORWARD_CAN_SOCKET_START="" # This command string is not packed into a variable
FORWARD_CAN_SOCKET_STOP="killall $MENU_PY"
#VNC_SERVER_START="" # This command string is not packed into a variable
#VNC_SERVER_STOP="" # This command string is not packed into a variable
HOTSPOT_START="$PATH_TO_SH_SCRIPTS/./$START_HOTSPOT_SH"
HOTSPOT_STOP="$PATH_TO_SH_SCRIPTS/./$STOP_HOTSPOT_SH"

# Start menu (to save space there is no indentation for the
# content of the while loop)
while :
do
# Clear screen
clear
echo "================================================="
echo "|            >>>  System Control  <<<           |"
echo "================================================="
echo "| CAN interface control:  . . . . . . . . . [1] |"
echo "| Socket/HTTP server control: . . . . . . . [2] |"
echo "| Forward CAN->Socket control:  . . . . . . [3] |"
echo "| VNC server control: . . . . . . . . . . . [4] |"
echo "| Wireless HotSpot control: . . . . . . . . [5] |"
echo "| Exit application: . . . . . . . . . . . . [x] |"
echo "================================================="

# Check if this script is executed as root and exit if not
if [ "$EUID" -ne 0 ]; then
	echo -e "\nPlease execute this script as root!"
	
	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to exit"
	clear
  exit
fi

# Read user input
echo -n "Input: "
read -e option
#echo ""

############
# Option 1 #
############
if [ "$option" == "1" ]; then
	while :
	do
		clear
		echo "================================================="
		echo "|             >>>  CAN Control  <<<             |"
		echo "================================================="
		echo "| Bring up CAN interface: . . . . . . . . . [1] |"
		echo "| Bring down CAN interface: . . . . . . . . [2] |"
		echo "| Enable start at boot: . . . . . . . . . . [3] |"
		echo "| Disable start at boot:. . . . . . . . . . [4] |"
		echo "| Back to main menu:  . . . . . . . . . . . [x] |"
		echo "================================================="
		echo -ne "Input: "
		read -e suboption
		echo ""
		if [ "$suboption" == "1" ]; then
			echo -e "Bringing up CAN interface with the following command:\n$ $CAN_UP"
			$CAN_UP
		elif [ "$suboption" == "2" ]; then
			echo -e "Bringing down CAN interface with the following command:\n$ $CAN_DOWN"
			$CAN_DOWN
		elif [ "$suboption" == "3" ]; then
			echo -e "Enabling start at boot with the following command:\nsystemctl enable myCAN"
			systemctl enable myCAN
		elif [ "$suboption" == "4" ]; then
			echo -e "Disabling start at boot with the following command:\nsystemctl disable myCAN"
			systemctl disable myCAN
		elif [ "$suboption" == "x" ]; then
			echo "Back to main menu"
			break
		else
			echo "Wrong input!"
		fi

	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to continue"

	# End of while loop
	done

############
# Option 2 #
############
elif [ "$option" == "2" ]; then
	while :
	do
		clear
		echo "================================================="
		echo "|     >>>  Socket/HTTP Server Control  <<<      |"
		echo "================================================="
		echo "| Start socket/HTTP server: . . . . . . . . [1] |"
		echo "| Stop socket/HTTP server:  . . . . . . . . [2] |"
		echo "| Enable start at boot: . . . . . . . . . . [3] |"
		echo "| Disable start at boot:. . . . . . . . . . [4] |"
		echo "| Back to main menu:  . . . . . . . . . . . [x] |"
		echo "================================================="
		echo -ne "Input: "
		read -e suboption
		echo ""
		if [ "$suboption" == "1" ]; then
			echo -e "Starting the Socket/HTTP server with the following command:\n$ $SOCKET_SERVER_START &"
			# The parentheses cause a sub-shell to be spawned. This sub-shell then changes its working directory
			# to the given path, then executes node from there. After the program exits, the sub-shell terminates
			# returning you to your prompt of the parent shell, in the directory you started from.
			# To avoid running the program without having changed the directory, e.g. when having misspelled
			# the path, make the execution of node conditional, by using "&&".
			# To avoid having the subshell waste memory while node executes, call it via exec
			(cd $PATH_TO_SERVER_JS && exec $SOCKET_SERVER_START &)
		elif [ "$suboption" == "2" ]; then
			echo -e "Stopping the Socket/HTTP server with the following command:\n$ $SOCKET_SERVER_STOP"
			$SOCKET_SERVER_STOP
		elif [ "$suboption" == "3" ]; then
			echo -e "Enabling start at boot with the following command:\nusystemctl enable mySocket"
			systemctl enable mySocket
		elif [ "$suboption" == "4" ]; then
			echo -e "Enabling start at boot with the following command:\nsystemctl disable mySocket"
			systemctl disable mySocket
		elif [ "$suboption" == "x" ]; then
			echo "Back to main menu"
			break
		else
			echo "Wrong input!"
		fi

	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to continue"

	# End of while loop
	done


############
# Option 3 #
############
elif [ "$option" == "3" ]; then
	while :
	do
		clear
		echo "================================================="
		echo "|     >>>  Forward CAN->Socket Control  <<<     |"
		echo "================================================="
		echo "| Start forwarding CAN->Socket: . . . . . . [1] |"
		echo "| Stop forwarding CAN->Socket:  . . . . . . [2] |"
		echo "| Enable start at boot: . . . . . . . . . . [3] |"
		echo "| Disable start at boot:  . . . . . . . . . [4] |"
		echo "| Back to main menu:  . . . . . . . . . . . [x] |"
		echo "================================================="
		echo -ne "Input: "
		read -e suboption
		echo ""
		if [ "$suboption" == "1" ]; then
			echo -e "Starting forwarding frames (CAN->Socket) with the following command:"
			echo -e "(cd $PATH_TO_MENU_PY && runuser pi -c $MENU_PY -f &)"
			# The parentheses cause a sub-shell to be spawned. This sub-shell then changes its working directory
			# to the given path, then executes the python script from there. After the program exits, the sub-shell
			# terminates, returning you to your prompt of the parent shell, in the directory you started from.
			# To avoid running the program without having changed the directory, 
			# e.g. when having misspelled the path, make the execution of node conditional, by using "&&"
			# To run the python script as user pi, although this shell script is executed as root, use the
			# command runuser. Otherwise an error occurs (NotImplementedError: Invalid CAN Bus Type - None)
			# To pass over the executable python scipt as command the construct '"'" command "'" [options] ' are
			# used. The "&" only makes the command be executed in the background.
			(cd $PATH_TO_MENU_PY && runuser pi -c '"'"./$MENU_PY"'" -f &')
		elif [ "$suboption" == "2" ]; then
			echo -e "Stopping forwarding frames (CAN->Socket) with the following command:\n$ $FORWARD_CAN_SOCKET_STOP"
			$FORWARD_CAN_SOCKET_STOP
		elif [ "$suboption" == "3" ]; then
			echo -e "Enabling start at boot with the following command:\nsystemctl enable myForward"
			systemctl enable myForward
		elif [ "$suboption" == "4" ]; then
			echo -e "Disabling start at boot with the following command:\nsystemctl disable myForward"
			systemctl disable myForward
		elif [ "$suboption" == "x" ]; then
			echo "Back to main menu"
			break
		else
			echo "Wrong input!"
		fi
	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to continue"

	# End of while loop
	done

############
# Option 4 #
############
elif [ "$option" == "4" ]; then
	while :
	do
		clear
		echo "================================================="
		echo "|          >>>  VNC Server Control  <<<         |"
		echo "================================================="
		echo "| Start VNC server: . . . . . . . . . . . . [1] |"
		echo "| Stop VNC server:  . . . . . . . . . . . . [2] |"
		echo "| Enable start at boot: . . . . . . . . . . [3] |"
		echo "| Disable start at boot:  . . . . . . . . . [4] |"
		echo "| Back to main menu:  . . . . . . . . . . . [x] |"
		echo "================================================="
		echo -ne "Input: "
		read -e suboption
		echo ""
		if [ "$suboption" == "1" ]; then
			echo -e "Starting VNC server with the following command:"
			echo -e "runuser pi -c vncserver :2 -geometry 1280x1024 -depth 16 -pixelformat rgb565"
			# This is only a quick solution. The command string should be moved into a variable and 
			# declared at the beginning of this file. The problem is the passing of the command to the
			# program runuser with the flag -c. Here some difficulties appeared. At option 3 it worked 
			# with one option
			runuser pi -c "vncserver :2 -geometry 1280x1024 -depth 16 -pixelformat rgb565"
		elif [ "$suboption" == "2" ]; then
			echo -e "Stopping VNC server with the following command:"
			echo -e "runuser pi -c vncserver -kill :2"
			runuser pi -c "vncserver -kill :2"
		elif [ "$suboption" == "3" ]; then
			echo "This functionality has not been implemented yet"

		elif [ "$suboption" == "4" ]; then
			echo "This functionality has not been implemented yet"

		elif [ "$suboption" == "x" ]; then
			echo "Back to main menu"
			break
		else
			echo "Wrong input!"
		fi
	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to continue"

	# End of while loop
	done

############
# Option 5 #
############
elif [ "$option" == "5" ]; then
	while :
	do
		clear
		echo "================================================="
		echo "|      >>>  Wireless HotSpot control  <<<       |"
		echo "================================================="
		echo "| Start wireless hotspot: . . . . . . . . . [1] |"
		echo "| Stop wireless hotspot:  . . . . . . . . . [2] |"
		echo "| Enable start at boot: . . . . . . . . . . [3] |"
		echo "| Disable start at boot:  . . . . . . . . . [4] |"
		echo "| Back to main menu:  . . . . . . . . . . . [x] |"
		echo "================================================="
		echo -ne "Input: "
		read -e suboption
		echo ""
		if [ "$suboption" == "1" ]; then
			echo -e "Starting the wireless hotspot by executing the following script:\n$HOTSPOT_START"
			$HOTSPOT_START
		elif [ "$suboption" == "2" ]; then
			echo -e "Stopping the wireless hotspot by executing the following script:\n$HOTSPOT_STOP"
			$HOTSPOT_STOP
		elif [ "$suboption" == "3" ]; then
			echo "Enabling start at boot with the following commands:"
			echo "systemctl enable myHotSpot"
			systemctl enable myHotSpot
		elif [ "$suboption" == "4" ]; then
			echo "Disabling start at boot with the following commands:"
			echo "systemctl disable myHotSpot"
			systemctl disable myHotSpot
			# Stopping wireless hotspot resets the network adapter
			$HOTSPOT_STOP
		elif [ "$suboption" == "x" ]; then
			echo "Back to main menu"
			break
		else
			echo "Wrong input!"
		fi
	# Wait for user to press ENTER to show previous output
	echo ""
	read -s -p "Press ENTER to continue"

	# End of while loop
	done

#############
# Exit menu #
#############
elif [[ "$option" == "x" || "$option" == "X" ]]; then
 	# Exit while loop and end menu
 	break
else
 	echo -e "\nWrong input!"
fi

# Wait for user to press ENTER to show previous output
echo ""
read -s -p "Press ENTER to continue"

# Alternative with any key:
#read -n 1 -s -r -p "Press any key to continue"

# End of while loop (of main menu)
done

# Clear screen
clear
