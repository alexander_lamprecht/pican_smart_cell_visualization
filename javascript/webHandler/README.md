# What is in here?

This folder contains all JavaScript based applications for the visualization of the cell status and for the communication between Python based service and remote web client(s).

Folder/File | Description
------ | ------
[`node_modules`](node_modules) | This folder contains all necessary  libraries and was generated automatically after executing `npm init`.
[`public`](public) | This folder contains the client-side JavaScript source code.
[`package-lock.json`](package-lock.json) | This file contains information about this project and was generated automatically after executing `npm init`.
[`package.json`](package.json) | This file contains information about this project and was generated automatically after executing `npm init`.
[`server.js`](server.js) | This file represents the server-side JavaScript application and contains a socket and HTTP server as well as a forwarding service for specific received socket messages.
