
/*---------------------------------------------------------------------------------
	Filename:        sketch.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   11 Oct 2017
	Comments:        This is a javascript file containing the main client script
                   for the raw message output
---------------------------------------------------------------------------------*/

// DOM elements
var check, input, sendButton, refreshButton, homeButton, text1, text2, header;

// Define some references
var xRef = 10;
var yRef = 10;
var xSpace = 10;
var ySpace = 30;
var y_pos1, y_pos2;

// Get ip address of raspberry pi
var ip_add = location.host;
var port = "80";

// Main p5.js setup function (is called only once at the beginning)
function setup() {

  // create canvas
  createCanvas(windowWidth, windowHeight);

	// Connect to socket server (on Android it only works if you use the IP address here)
	//socket = io.connect("http://raspberrypi.local:80");
	socket = io.connect(ip_add+":"+port);
	// Use the following line instead, if wireless hotspot is used
	//socket = io.connect("http://192.168.0.1:80");

	// Create a few DOM elements
  homeButton = createButton("Home");
  homeButton.position(xRef, yRef);
  homeButton.mouseClicked(homePage);

  refreshButton = createButton("Refresh");
  refreshButton.position(homeButton.x + homeButton.width + xSpace, yRef);
  refreshButton.mousePressed(reloadPage);

	header = createElement("h2", "Raw Message View");
	header.position(refreshButton.x + 150, -10);

  input = createInput("");
  input.position(xRef, homeButton.y + 3 * ySpace);

	text1 = createP("Put in data to be sent");
	text1.position(xRef, input.y - (input.height + 10));

	text2 = createP("Sent data:")
	text2.position(xRef, input.y + input.height);

  sendButton = createButton("Send");
  sendButton.position(input.x + input.width, input.y);
  sendButton.mouseClicked(sendData);

  check = createCheckbox("Listen and print incoming messages:", false);
  check.position(sendButton.x + 120, input.y);
  check.changed(getData);

	// Set initial values
  y_pos1 = text2.y + ySpace;
  y_pos2 = check.y + ySpace;
}

// Main p5.js draw function (is called repeatedly)
function draw() {
	background(255);
}

// This function sends the data of the input when the send button is pressed
function sendData() {

	var txt;

	// Create DOM element to be printed
	txt = createP(input.value());

	// Increment the position after printing
	txt.position(xRef, y_pos1);
  y_pos1 += ySpace;

	// Send the data to the socket with specified event name
  socket.emit("msg", input.value());
}

// This function receives data and prints it if the check box is enabled
function getData() {

	// Only listen if check box is enabled
  if (check.checked()) {
    socket.on("vol", printData);
  }
  else {
    socket.off("vol");
  }
	// Function for printing the data (data contains a list consisting of 
	//the cell ID in data[0] and the respective cell voltage in data[1] 
  function printData(data) {

		// DOM element
		var text;

		// Create DOM element to be printed
		text = "ID" + data[0] + ": " + data[1].round(2) + " V";
		text = createP(text);
		text.position(check.x, y_pos2);

		// Increment y-position of text
    y_pos2 += ySpace;
  }
}

// Dynamically adjust the canvas size to the window size as soon as window size
// is changed
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

// Callback function for the home button
function homePage() {
	window.location.pathname = "../";
}

// Callback function for the refresh button
function reloadPage() {
  location.reload();
}

// A function to round floating numbers
Number.prototype.round = function(places) {
  return +(Math.round(this + "e+" + places)  + "e-" + places);
}
