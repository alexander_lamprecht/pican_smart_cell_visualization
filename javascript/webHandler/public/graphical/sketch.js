
/*---------------------------------------------------------------------------------
	Filename:        sketch.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   11 Oct 2017
	Comments:        This is a javascript file containing the main client script
                   for the graphical visualization
---------------------------------------------------------------------------------*/
// Get ip address of raspberry pi
var ip_add = location.host;
var port = "80";

function setup() {

	// Create a canvas for drawing
  createCanvas(windowWidth, windowHeight);

	// Connect to socket server (on Android and Windows you have to connect by using the IP address)
	connectSocket(ip_add, port);
	//connectSocket('raspberrypi.local', 80);
	// Use the following line instead, if wireless hotspot is used
	//connectSocket('192.168.0.1', 80);

	// Initialize cells
	initCells();

	// Initialize settings GUI
	setupGUI();

	// Create different DOM elements (buttons, check boxes, etc.)
	createDOMelements();

	// To ensure correct position at start up
	repositionDOMelements();

	// Load the logo
	loadLogo();

  // Don't loop automatically (use redraw() to only draw if wanted)
  //noLoop();
}


function draw() {

  // Clear the canvas
  clear();

	// Draw the cells
	drawCells();

  // Draw logo
	drawLogo();
}
