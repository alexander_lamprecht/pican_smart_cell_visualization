
/*---------------------------------------------------------------------------------
	Filename:        sketch.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   28 Sep 2017
	Comments:        This is a javascript file containing the main client script
                   for showing the system architecture
---------------------------------------------------------------------------------*/

// DOM elements (other names than in myGUI.js)
var myHomeButton, myHeader;

// Image variables
var imagePath = "../src/SystemArchitecture-V03-Link.png";
var myImage, myImageWidth, myImageHeight;

// Set the image ratio manually (dirty solution, to be fixed)
var myImageRatio = 1125/690;

// Margin on each side of the image
var myMargin = 10;

// Offest because of home button and header
var yOffset = 80;

function setup() {

  // create canvas
  createCanvas(windowWidth, windowHeight);

	// Create a few DOM elements
  myHomeButton = createButton("Home");
  myHomeButton.position(10, 10);
  myHomeButton.mouseClicked(homePage);

	myHeader = createElement("h2", "System Architecture of Visualization");
	myHeader.position(windowWidth/2 - (myHeader.size()).width/2, (2*yRef));

	// Load the logo
	loadLogo();

	// Load image
  myImage = loadImage(imagePath);

	//myImageRatio = (myImage.size()).width/(myImage.size()).height;

	// Initialize image size
	myImageWidth = windowWidth - 2 * myMargin ;
	myImageHeight = myImageWidth/myImageRatio;
}

function draw() {

  // Clear the canvas
  clear();

	// Draw the image
	drawMyImage();

	// Draw the logo
	drawLogo();
}

function drawMyImage() {

	// Adjust the image size according to the window size
	myImageWidth = windowWidth - 2 * myMargin;
	myImageHeight = myImageWidth/myImageRatio;

	// Make sure, that always the whole image is visible (regardless of the window ratio)
	if(windowHeight < myImageHeight + yOffset + myMargin) {
		myImageHeight = windowHeight - yOffset - myMargin;
		myImageWidth = myImageHeight * myImageRatio;
	}

	// Draw image
  image(myImage, windowWidth/2 - myImageWidth/2, yOffset, myImageWidth, myImageHeight);

	// Reposition the header
	myHeader.position(width/2 - (myHeader.size()).width/2, (2*yRef));
}


// dynamically adjust the canvas to the window
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function homePage() {
	window.location.pathname = "../";
}

