# What is in here?

This folder contains the client-side JavaScript source code.

Folder/File | Description
------ | ------
[`graphical`](graphical) | This folder contains the JavaScript source code for the "*Graphical Visualization View*".
[`libraries`](libraries) | This folder contains all necessary JavaScript libraries.
[`rawmessages`](rawmessages) | This folder contains the JavaScript source code for the "*Raw Messages View*".
[`src`](src) | This folder contains all sources, like images or tables.
[`systemarchitecture`](systemarchitecture) | This folder contains the JavaScript source code for the "*System Architecture View*".
[`index.html`](index.html) | This file is opened initially when accessing the web interface via browser.
