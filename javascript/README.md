# What is in here?

This folder contains all JavaScript based applications for the visualization of the cell status and for the communication between Python based service and remote web client(s).

Folder | Description
------ | ------
[`webHandler`](webHandler) | This folder contains the whole JavaScript source code, both server and client side scripts and the necessary libraries.
