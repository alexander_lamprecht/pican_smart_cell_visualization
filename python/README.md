# What is in here?

This folder contains the Python based scripts to communicate with the PiCAN2 add-on board and the JavaScript based web interface via the WebSocket protocol.

Folder/File | Description
------ | ------
[`can-test`](can-test)              | This folder contains simple Python scripts to test if the communication via the PICAN2 add-on board works. It is recommended to test these two scripts with the Peak CAN USB adapter and not on the Smart Cell Demonstrator. It also has a script to enable/disable balancing cells.
[`menu.py`](menu.py)                | This is the main Pyhton script which starts a menu and provides different options to choose from. This way all the different functionalities can be tested separately without interdependencies.
[`myCAN.py`](myCAN.py)              | This file contains a class which represents the CAN interface and uses the `python-can` package (see [here](http://python-can.readthedocs.io/en/latest/)).
[`myFunctions.py`](myFunctions.py)  | This file contains mixed functions used in the main Python script `menu.py`.
[`myMessage.py`](myMessage.py)      | This file contains the definition of the specific CAN message layout.
