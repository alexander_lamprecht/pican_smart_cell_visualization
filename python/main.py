#!/home/pi/.virtualenv/bin/python

# ---------------------------------------------------------------------------------
# Filename:        main.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisors:     Alexander Lamprecht
# Last modified:   17 Sep 2018
# Comments:        This file contains the main python script that updates the
#                  data coming from bus can to in a log for each cell and in
#                  parallel reads out and respond to the request coming from socket
# ---------------------------------------------------------------------------------

# To support the new print function of python3
from __future__ import print_function

# Import packages
import os
import sys
import time
import threading
import csv
import json
import datetime
import copy
import numpy as np
from scipy import interpolate

from socketIO_client import SocketIO

from myFunctions import *
from myCAN import *
from myMessage import *
from mySoCCalc import *
from myCell import *


# LOCK to prevent reader from starving in multi-threading environment
LOCK = threading.Lock()

# Global variable definitions
HOST = 'localhost'
# Use the following line instead, if using the wireless hotspot
# HOST = '198.168.0.1'

# Set the default port for HTTP (so it does not has to be entered in the address line of the browser)
# set PORT as "80", if accessing from other machine
PORT = 80
try:
    print("\nConnecting to the socket server...\n")
    print("If this takes more than a few seconds, the server is probably not running.")
    print("(Re)Start the socket server or hit Ctrl-C to terminate this application.")
    SOCKET = SocketIO(HOST, PORT)
    # print("Connected to server")
except:
    print("\nApplication closed.")
    exit()

# Setup CAN communication
# channel = 'can0'
# bustype = 'socketcan'
CAN_ID = 0x7de
CAN_BUS = myCAN(ID=CAN_ID)

# Check if CAN interface is already brought up and if yes set it up
if CAN_BUS.check() == 1:
    CAN_BUS.up = True
else:
    CAN_BUS.up = False

# global counter of sent messaged to the socket
msg_cnt = 0

# global array of cells
CELLS = []  # intialize cells list
NUM_cells = 5
for i_cell in range(NUM_cells):
    CELLS.append(Cell())

# csv files for storing log data
LOG_SEC = 'LOG_SEConds.csv'
LOG_MIN = 'LOG_MINutes.csv'


def msg_count():
    """docstring"""
    global msg_cnt
    msg_cnt += 1
    return '[' + str(msg_cnt) + ']'


def filter_log(inp):
    """docstring"""
    # the function receives data in form of a list from socket.
    # format: #input = [CellID, total time, resolution (time interval between 2 entries)]
    [cell_id, t_total, t_reso] = inp
    d_sec = []
    d_min = []
    # print(cell_id, t_total, t_reso)

    LOCK.acquire()

    # load both csv files in a python list
    with open(LOG_SEC, 'r') as f_read:
        next(f_read)    # skip header
        reader = csv.reader(f_read)
        for row in reader:
            row_data = [row[0]]+[json.loads(row[1])]
            d_sec.append(row_data)

    with open(LOG_MIN, 'r') as f_read:
        next(f_read)    # skip header
        reader = csv.reader(f_read)
        for row in reader:
            row_data = [row[0]]+[json.loads(row[1])]
            d_min.append(row_data)

    LOCK.release()

    def val_return(type_, input_, data_s, data_m):
        """docstring"""
        # this function takes input (type_ = 'time'/'voltage'/'current'/'soc',
        # input_ = "[CellID, total time, resolution or time interval between 2 entries]",
        # data_s and data_m are data from LOG_SEConds and LOG_MINutes csv files)
        # and returns respective array

        [cell_id, t_total, t_reso] = input_  # time is in seconds
        # use deepcopy when data structure is nested
        data_min = copy.deepcopy(data_m)
        data_sec = copy.deepcopy(data_s)
        data_total = data_min + data_sec

        # convert time into epoch form, for easier implementation
        for i in range(len(data_total)):
            t_key = (data_total[i][0])
            t_epoch = [int(elements) for elements in (t_key.split(
                " ")[0].split("-")) + t_key.split(" ")[1].split(":")]
            t_key1 = (datetime.datetime(t_epoch[0], t_epoch[1], t_epoch[2], t_epoch[3],
                                        t_epoch[4], t_epoch[5]) - datetime.datetime(1970, 1, 1)).total_seconds()
            data_total[i][0] = t_key1

        # store desired timestamps in t_array
        t_now = max(data_total[i][0] for i in range(len(data_total)))
        num_entry = int(round(t_total/t_reso))
        t_array = []
        for i in range(num_entry):
            t_array.append(t_now-(i*t_reso))

        for i in range(len(t_array)):
            idx = 0
            min_diff = abs(t_array[i]-data_total[0][0])
            for j in range(len(data_total)):
                if abs(t_array[i]-data_total[j][0]) < min_diff:
                    idx = j
                    min_diff = abs(t_array[i]-data_total[j][0])
            t_array[i] = data_total[idx][0]

        # to get unique entries in t_array
        t_array_set = set(t_array)
        t_array = list(t_array_set)
        t_array.sort()

        # initialize output array
        outp = []

        if type_ == 'time':
            for i in range(len(t_array)):
                t_array[i] = time.strftime(
                    '%Y-%m-%d %H:%M:%S', time.gmtime(t_array[i]))

            outp = t_array
        elif type_ == 'voltage':
            vol_array = []
            for t in t_array:
                for row in data_total:
                    if t == row[0]:
                        vol_array.append(row[1][cell_id]["voltage"])

            outp = vol_array
        elif type_ == 'current':
            curr_array = []
            for t in t_array:
                for row in data_total:
                    if t == row[0]:
                        curr_array.append(row[1][cell_id]["current"])

                outp = curr_array
        elif type_ == 'soc':
            soc_array = []
            for t in t_array:
                for row in data_total:
                    if t == row[0]:
                        soc_array.append(row[1][cell_id]["soc"])

            outp = soc_array
        else:
            print("Enter a valid type-entry")

        return outp


    # initialize data
    data = {
        "t_arr": val_return('time', inp, d_sec, d_min),
        "cells": {
        },
    }

    # if cell_id is not in the range, return data of all the cells
    if cell_id in range(NUM_cells):
        cell_id_list = [cell_id]
    else:
        cell_id_list = [i for i in range(NUM_cells)]

    for cell_id in cell_id_list:
        inp = [cell_id, t_total, t_reso]
        data["cells"][cell_id] = {
            "V": val_return('voltage', inp, d_sec, d_min),
            "I": val_return('current', inp, d_sec, d_min),
            "SOC": val_return('soc', inp, d_sec, d_min),
        }

    return data


def listen_for_CAN_msg():
    """docstring"""
    running = True
    CAN_BUS.addListener()
    CAN_BUS.startNotifier()

    while running:
        CANmsg = CAN_BUS.listener.get_message()
        if CANmsg is not None:
            origin = (CANmsg.arbitration_id) & 0xFF
            target = (CANmsg.arbitration_id >> 8) & 0xFF
            msgType = (CANmsg.arbitration_id >> 16) & 0xFFFF

            can_msg_handler(msgType, origin, target, CANmsg.data)


def can_msg_handler(msgType, origin, target, data):
    """docstring"""
    # defines the actions for incoming messages

    if msgType in [MSG_TYPE.VOLTAGE]:
        CELLS[origin].set_voltage(round(byte2float(data), 3))
        CELLS[origin].compute_soc()
        return

    if msgType in [MSG_TYPE.SOC]:
        print('SOC:', round(byte2float(data), 3))
        return

    if msgType in [MSG_TYPE.SEND_ACK]:
        # balancing started. Charge transferred from origin to target
        SOCKET.emit('bal_start', (origin, target))
        print(msg_count(), "bal_start broadcast sent:", (origin, target))
        CELLS[origin].set_current(-1)
        CELLS[target].set_current(1)
        return

    if msgType in [MSG_TYPE.UNBLOCK]:
        # balancing stopped from "cell_from" to "cell_to".
        [cell_from, cell_to] = list(data)
        SOCKET.emit('bal_stop', (cell_from, cell_to))
        print(msg_count(), "bal_stop broadcast sent:", (cell_from, cell_to))
        CELLS[cell_from].set_current(0)
        CELLS[cell_to].set_current(0)
        return

    # add additional handlers here


def log_data():
    """docstring"""
    # function to periodically write the current data to a log file

    # initialize both log files if not present
    # with open ensures that the file will close after execution
    try:
        with open(LOG_SEC, 'r') as f:
            pass

    except IOError:
        with open(LOG_SEC, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(["Timestamp", "Data"])
            print("new LOG_SEC.csv file created")
    try:
        with open(LOG_MIN, 'r') as f:
            pass

    except IOError:
        with open(LOG_MIN, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(["Timestamp", "Data"])
            print("new LOG_MIN.csv file created")

    # counter for time(in seconds)
    t_cnt = [0]

    # number of recent data needed in second resolution( 1 min in this case)
    n_sec = [60]

    # number of recent data entries needed with minute resolution( 4 hours in this case)
    n_min = [240]

    def log_update():
        """docstring"""
        # global LOCK
        LOCK.acquire()
        data_cells = []
        for i in range(Cell.num_of_cells):
            json_dict = {
                'id': i,
                'voltage': CELLS[i].get_voltage(),
                'current': CELLS[i].get_current(),
                'temp': CELLS[i].get_temperature(),
                'soc': CELLS[i].get_soc()
                }
            data_cells.append(json_dict)
        json_data_cells = json.dumps(data_cells, sort_keys=True)

        def update_LOG_SEC():
            """docstring"""
            data = []
            with open(LOG_SEC, 'r') as f_read:
                reader = csv.reader(f_read)
                for row in reader:
                    data.append(row)

            data.append([str(datetime.datetime.now()).split('.')[0], json_data_cells])

            # subtracting 1 because header row is there
            num_entry = sum(1 for row in data) - 1
            if num_entry >= n_sec[0]:
                # to store headings in csv and updating data for recent n_sec entries
                data = [data[0]] + data[-n_sec[0]:]

            with open(LOG_SEC, 'w') as f_write:
                writer = csv.writer(f_write)
                writer.writerows(data)

        def update_LOG_MIN():
            """docstring"""
            if t_cnt[0] % 60 == 0:
                data = []
                with open(LOG_MIN, 'r') as f_read:
                    reader = csv.reader(f_read)
                    for row in reader:
                        data.append(row)

                data.append([str(datetime.datetime.now()).split('.')[0], json_data_cells])

                # subtracting 1 because 1 row is header
                num_entry = sum(1 for row in data) - 1

                if num_entry >= n_min[0]:
                    # to store headings in csv and updating data for recent n_min entries
                    data = [data[0]] + data[-n_min[0]:]

                with open(LOG_MIN, 'w') as f_write:
                    writer = csv.writer(f_write)
                    writer.writerows(data)
            else:
                pass

        update_LOG_SEC()
        update_LOG_MIN()
        t_cnt[0] += 1
        LOCK.release()

        # log_update function will be excecuted after everyone second
        threading.Timer(1, log_update).start()

    # trigger the log_update function  once to initialize log files and time counter
    log_update()


def attach_socket_handlers():
    """docstring"""
    # this thread keeps waiting for socket requests and calls functions when requests are received.
    def emit_log(data):
        """docstring"""
        print(msg_count(), "Log request received from cell number:", data)
        req_id = data['target_id']
        print(req_id)
        log_data_ = json.dumps(filter_log(data['msg']))  # returns cell log in json format
        print(log_data_)
        SOCKET.emit('log_res', {'target_id': req_id, 'msg': log_data_})

    def emit_no_cells():
        """docstring"""
        SOCKET.emit('num_of_cells_res', Cell.num_of_cells)
        print(msg_count(), " um_of_cells_res response sent:", Cell.num_of_cells)

    def emit_enable_bal():
        """docstring"""
        enable_msg = can.Message(arbitration_id=0x0030ff00, data=[1], extended_id=True)
        CAN_BUS.bus.send(enable_msg)
        print(msg_count(), "balancing enable CAN message sent")

    def emit_disable_bal():
        """docstring"""
        disable_msg = can.Message(arbitration_id=0x0030ff00, data=[0], extended_id=True)
        CAN_BUS.bus.send(disable_msg)
        print(msg_count(), "balancing disable CAN message sent")

    if SOCKET.connected is True:
        # attach message handlers
        SOCKET.on("log_req", emit_log)
        SOCKET.on("num_of_cells_req", emit_no_cells)
        SOCKET.on("enable_bal_req", emit_enable_bal)
        SOCKET.on("disable_bal_req", emit_disable_bal)
        SOCKET.wait()


def periodic_emit():
    """emits the current cell values periodically"""
    emit_period = 1  # time in seconds between emits

    def emit_data(data):
        """docstring"""
        json_dict = {
            "time": datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'),
            "cell_data": {}
        }
        cell_dict = {}
        for cell in data:
            cell_dict[cell.get_id()] = {
                "temperature": cell.get_temperature(),
                "current": cell.get_current(),
                "voltage": cell.get_voltage(),
                "soc": cell.get_soc()
            }
            # print(cell.get_id(), cell.get_soc(), cell.target_soc, cell.soc_grad_min, cell.soc_grad)
        json_dict["cell_data"] = cell_dict
        SOCKET.emit('current_val_br', json.dumps(json_dict))
        print(msg_count(), "periodic_emit broadcasted")

    emit_data(CELLS)
    threading.Timer(emit_period, periodic_emit).start()


# start the program if CANbus is ON
if CAN_BUS.up is True:

    # create threads for different tasks
    thread_1 = threading.Thread(target=listen_for_CAN_msg)
    log_data()
    thread_2 = threading.Thread(target=attach_socket_handlers)
    thread_3 = threading.Timer(1.0, periodic_emit)

    thread_1.start()
    thread_2.start()
    thread_3.start()

    thread_1.join()
    thread_2.join()


else:
    print("bring up the CAN first")
