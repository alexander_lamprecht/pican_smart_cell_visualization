#!/home/pi/.virtualenv/bin/python

#---------------------------------------------------------------------------------
# Filename:        menu.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba, Hannes Bohnengel
# Supervisors:     Alexander Lamprecht
# Last modified:   04 Oct 2017
# Comments:        This is a test menu-based application to test all components
#                  first isolated and then in combination. If it is exectued
#                  without arguments, then a menu is printed to the terminal where
#                  the user can select different options. It also can be executed
#                  with arguments to choose and execute an option of the menu.
#                  Usage:
#                  $ ./test.py [options]
#                  Options:
#                     -h, --help      show this help message and exit
#                     -f, --forward   forward voltage values (CAN->Socket) in silent mode
#                     -h, --help                   show this help message and exit
#                     -s OPTION, --silent=OPTION   Execute an option of the menu
#                                                  in silent mode (without prints)
#
#---------------------------------------------------------------------------------

# To support the new print function of python3
from __future__ import print_function

# Import packages
import os
import sys
from socketIO_client import SocketIO
import optparse # for parsing commandline arguments
from myFunctions import *
from myCAN import *
from myMessage import *

# Create a parser for input arguments
parser = optparse.OptionParser()

# Create options
parser.add_option('-f', '--forward', action="store_true", dest="forward",
help="forward voltage values (CAN->Socket) in silent mode", default=False)

# Parse input arguments and store them
opt, args = parser.parse_args()

# Check if any options are given
if opt.forward == True: # and other options (if added) are False
	silent = True
#elif opt.[option] == True: # and other option are False
#	silent = True
elif opt.forward == False: # and other options (if added) are also False
	silent = False
else:
	# Print warning and end application
	print("Usage: test.py [options]\n")
	print("test.py: error: please use only one option at a time")
	print("opt.forward = {}".format(opt.forward))
	exit()

if silent == True:
	# Suppress printouts to stdout
	f = open(os.devnull, 'w')
	sys.stdout = f



# Global variable definitions
myHost = 'localhost'
# Use the following line instead, if using the wireless hotspot
#myHost = '198.168.0.1'

# Set the default port for HTTP (so it does not has to be entered in the address line of the browser)
#myPort = 80
#for trial
myPort = 80

# Printing some information
if silent == False:
	os.system('clear')
	print("\nConnecting to the socket server...\n")
	print("If this takes more than a few seconds, the server is probably not running.")
	print("(Re)Start the socket server or hit Ctrl-C to terminate this application.")

#try:
mySocket = SocketIO(myHost, myPort)
print("Connected")
#except:
#	print("\nApplication closed.")
#	exit()

# Can communication
channel = 'can0'
bustype = 'socketcan'
ID = 0x7de
myCANbus = myCAN(channel, bustype, ID)

# Check if CAN interface is already brought up and if yes set it up 
if myCANbus.check() == 1:
	myCANbus.up = True
else:
	myCANbus.up = False

# Callback function for forwarding messages from web client to CAN bus
# This function is placed here, because it accesses both socket and
# CAN interface and therefore uses the myCANbus object and servers as
# callback function to a socketIO function
def forwardSocketCAN(args):
	# Convert the input to hex values
	msg = ascii2hex(unicode2ascii(args))
	# Check if message is valid
	if msg == -1:
		print("Please enter a valid hex value!")
	else:
		try:
			myCANbus.send(msg)
			print("{}".format(msg))
		except:
			print("Could not send message over CAN bus!")

# Clear terminal (cls for Windows)
if silent == False:
	os.system('clear')



while True:
        print("=================================================")
        print("|  >>>  Pi-CAN-Analyzer - Testing program  <<<  |")
        print("=================================================")
        print("| Connect to socket server: . . . . . . . . [1] |")
        print("| Send data to socket:  . . . . . . . . . . [2] |")
        print("| Listening to socket:  . . . . . . . . . . [3] |")
        print("| Disconnecting from socket server: . . . . [4] |")
        print("| Set up CAN interface: . . . . . . . . . . [5] |")
        print("| Send data via CAN:  . . . . . . . . . . . [6] |")
        print("| Listen to CAN bus:  . . . . . . . . . . . [7] |")
        print("| Enable bridge (Socket->CAN):  . . . . . . [8] |")
        print("| Enable bridge (CAN->Socket):  . . . . . . [9] |")
        print("| Forward cell status (CAN->Socket):  . . . [a] |")
        print("| Enable Balancing (testing): . . . . . . . [b] |")
        print("| End application:  . . . . . . . . . . . . [x] |")
        print("=================================================")
        if mySocket.connected == True:
                print("| Socket Server: {}:{}".format(myHost,myPort), end='')
        else:
                print("| Socket Server: Not connected!", end='')
        if myCANbus.up == True:
                print("\t|  CAN up:  [X] |")
        else:
                print("\t|  CAN up:  [ ] |")
        print("=================================================")
        # Wait for user input

        if silent == False:
                try:
                        i = raw_input("> ")
                except:
                        os.system('clear')
                        i = '00'
                        continue
        else:
                if opt.forward == True:
                        i = 'a'
                else:
                        i = '00'

        # Option 1
        if i == '1':
                if mySocket.connected == False:
                        try:
                                mySocket.connect()
                                print("\nConnected successfully.")
                        except:
                                print("\nConnection not possible!")
                else:
                        print("\nAlready connected to server {}:{}".format(myHost, myPort))

        # Option 2
        elif i == '2':
                if mySocket.connected == True :
                        print("\nSending string to socket with event name: 'msg'.")
                        data = raw_input("> ")
                        try:
                                mySocket.emit('msg',data)
                        except:
                                print("Something went wrong. Please reconnect and try again!")
                else:
                        print("\nPlease connect to socket server first!")

        # Option 3
        elif i == '3':
                if mySocket.connected == True :
                        print("\nListening to socket with event name 'msg'. \n(Ctrl-C to stop)")
                        try:
                                mySocket.on('msg', printResponse)
                                mySocket.wait()
                        except:
                                # This exception catches the termination with Ctrl-C
                                print("\nDisconnected.")
                else:
                        print("\nPlease connect to socket server first!")

        # Option 4
        elif i == '4':
                if mySocket.connected == True :
                        try:
                                mySocket.disconnect()
                                print("\nDisconnected successfully")
                        except:
                                        print("\nSomething went wrong while disconnecting!")
                else:
                        print("\nPlease connect to socket server first!")

        # Option 5
        elif i == '5':
                if myCANbus.up == False:
                        if myCANbus.setup() == 0:
                                print("\nCAN interface set up successfully.")
                                myCANbus.up = True
                else:
                        print("\nCAN interface already set up!")

        # Option 6
        elif i == '6':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                else:
                        print("\nSending data to CAN bus.")
                        print("Message format: 5FA312B14839D394")

                        try:
                                user_input = raw_input('\nEnter message: \n> ')
                        except:
                                os.system('clear')
                                continue

                        # Convert the input to list with hex values
                        msg = ascii2hex(unicode2ascii(user_input))

                        # Check if message contains invalid symbols
                        if msg == -1:
                                print("Please enter a valid hex value!")
                        else:
                                print("Message:", end='')
                                for i in msg:
                                        print(" %02X" % i, end='')
                                print("\nCAN-ID:  %X" %(myCANbus.ID))

                                # Send message over CAN bus
                                if myCANbus.send(msg) == 0:
                                        print("\nMessage sent successfully.")
                                else:
                                        print("Something went wrong while sending!")

        # Option 7
        elif i == '7':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                else:
                        # create a notifier which prints received messages to the terminal
                        myCANbus.startListener()

                        # The Listener can be stopped by any input
                        #
                        # !!! After stopping the listener one message could still be printed !!!
                        #
                        try:
                                raw_input("")
                                myCANbus.stopListener()
                        except:
                                # Catch a Ctrl-C
                                myCANbus.stopListener()

        # Option 8
        elif i == '8':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                elif mySocket.connected == False:
                        print("\nPlease connect to socket server first!")
                else:
                        print("\nEnabling bridge (Socket->CAN)")
                        try:
                                mySocket.on('msg', forwardSocketCAN)
                                mySocket.wait()
                        except:
                                # This exception catches the termination with Ctrl-C
                                print("\nDisconnected.")
                print("Disable bridge (Socket->CAN)")

        # Option 9
        elif i == '9':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                elif mySocket.connected == False:
                        print("\nPlease connect to socket server first!")
                else:
                        print("\nEnabling bridge (CAN->Socket)")

                        # Declaring an empty CAN message
                        CANmsg = can.Message()

                        # Listen for message and forwarding each of them to the socket
                        while(CANmsg != None):
                                try:
                                        CANmsg = myCANbus.bus.recv()
                                        if(CANmsg != None):
                                                print("{}".format(list(CANmsg.data)))
                                                # Sending data to socket
                                                mySocket.emit('msg', list(CANmsg.data))
                                except:
                                        print("\nDisable bridge (CAN->Socket).")
                                        break

        # Option a
        elif i == 'a' or i == 'A':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                elif mySocket.connected == False:
                        print("\nPlease connect to socket server first!")
                else:
                        print("\nForwarding whole CAN frames to socket server")

                        # Declaring an empty CAN message
                        CANmsg = can.Message()

                        # Listen for message and forwarding each of them to the socket
                        while(CANmsg != None):
                                frame = []

                                try:
                                        # Receive a message
                                        CANmsg = myCANbus.bus.recv()

                                        # Check if a message was received
                                        if(CANmsg != None):
                                                try:
                                                        # Decode message
                                                        origin  = (CANmsg.arbitration_id      ) & 0xFF
                                                        target  = (CANmsg.arbitration_id >>  8) & 0xFF
                                                        msgtype = (CANmsg.arbitration_id >> 16) & 0xFFFF


                                                        if(msgtype == MSG_TYPE.VOLTAGE):
                                                                # Build data frame
                                                                frame.append(origin)
                                                                frame.append(byte2float(CANmsg.data))
                                                                #frame.append(calculated SOC)


                                                                # Print forwarded cell ID and voltage (in one line)
                                                                print("(Cell-ID, Voltage) = (0x{0:02X}, ".format(frame[0]), end='')
                                                                print("{})".format(frame[1]), end="     \r")

                                                                try:
                                                                        # Sending frame to web handler
                                                                        mySocket.emit('vol', frame)
                                                                except:
                                                                        print("Error while sending frame!")
                                                                        print("Exception: {}".format(sys.exc_info()[0]))

                                                        '''
                                                        elif(msgtype == MSG_TYPE.STATUS_RESPONSE):
                                                                print("Received MSG_TYPE_STATUS_RESPONSE")
                                                        elif(msgtype == MSG_TYPE.SEND_ACK):
                                                                print("Received MSG_TYPE_SEND_ACK")
                                                        elif(msgtype == MSG_TYPE.SEND_ACK):
                                                                print("Received MSG_TYPE_SEND_REQ")
                                                        elif(msgtype == MSG_TYPE.SEND_ACK):
                                                                print("Received MSG_TYPE_SET_BALANCING")
                                                        elif(msgtype == MSG_TYPE.SEND_ACK):
                                                                print("Received MSG_TYPE_BLOCK")
                                                        '''


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# REMOVE THIS TRY - EXCEPT CONSTRUCT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                                except:
                                                        print("Error while building frame!")
                                                        print("Exception: {}".format(sys.exc_info()[0]))

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# REMOVE THIS TRY - EXCEPT CONSTRUCT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



                                except:
                                        print("\nDisable bridge (CAN->Socket).")
                                        print("Exception: {}".format(sys.exc_info()[0]))
                                        break

        # Option b
        elif i == 'b':
                if myCANbus.up == False:
                        print("\nPlease set up CAN interface first!")
                else:
                        print("\nEnabling Balancing by sending the following message:")
                        print("MsgType: 0x0030; Target: 0xFF; Origin: 0x00; Data: [0x01]")

                        msgtype = MSG_TYPE.SET_BALANCING
                        target = 0xFF
                        origin = 0x00

                        test = getExtID(msgtype, origin, target)

                        print("ExtID: %X" % test)

                        myCANbus.ID = test

                        msg = [1];

                        # Send message over CAN bus
                        if myCANbus.send(msg) == 0:
                                print("\nMessage sent successfully.")
                        else:
                                print("Something went wrong while sending!")

        # End program
        elif i == 'x' or i == 'X':
                os.system('clear')
                break

        # Wrong input
        else:
                print("\nWrong input!")

        if silent == False:
                try:
                        raw_input("\nPlease press ENTER to continue.")
                except:
                        print("")
        else:
                break

        os.system('clear')

# End of menu
