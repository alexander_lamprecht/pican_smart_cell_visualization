#---------------------------------------------------------------------------------
# Filename:        formA2B.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisor:      Alexander Lamprecht
# Last modified:   14 Aug 2018
# Comments:        This script converts csv in formA into csv in formB. formB is
#				   further utilsed by the scripts for soc calcultaion
#---------------------------------------------------------------------------------

import csv
from scipy.interpolate import interp1d
import copy
def interpol(voltage,temp,current,Dict):
    Voltage_list = [float(x[0]) for x in Dict[(temp,current)]]
    Soc_list = [float(x[1]) for x in Dict[(temp,current)]]
    f2 = interp1d(Voltage_list, Soc_list, kind='cubic')
    interp_voltage=f2(voltage)
    return interp_voltage

with open('./formA.csv','r') as f:
	reader = csv.reader(f)
	lookup_list = list(reader)[1:]

D = {}
Volt = []
for (T, I, V, Soc) in lookup_list:
 	if (T, I) not in D:
 		D[(T, I)] = []
 	D[(T, I)].append((V, Soc))
 	if V not in Volt:
 		# New voltage found, add in Volt
 		Volt.append(V)

original_Dict = copy.deepcopy(D)
print(original_Dict)


for (T, I, V, Soc) in lookup_list:
 	if V not in Volt:
 		# New voltage found, add in Volt
 		# Also add to all previous rows
 		for (t, i) in D:
 			if (t == T) and (i == I):
 				continue
 			else:
 				D[(t, i)].append((V, "nan")) #interpolated value
 	for v1 in Volt:
 		if v1 not in [x[0] for x in D[(T, I)]]:
 			D[(T, I)].append((v1, "nan"))  #interpolated value


print(original_Dict)

Volt= [round(float(element),3) for element in Volt]
with open('formB_int.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(["Temp","Curr","Voltages"])
    writer.writerow(["" ,"" ]+ sorted(Volt,reverse=True))
    for (T, I) in D.keys():
        writer.writerow([T, I] + [x[1] for x in sorted(D[(T, I)], key=lambda x: x[0],reverse=True)])

# Voltage_list = [x[0] for x in original_Dict[('30','1')]]
# Soc_list = [x[1] for x in original_Dict[('30', '1')]]
#
# print(Voltage_list)
# print(Soc_list)
# print(original_Dict==D)