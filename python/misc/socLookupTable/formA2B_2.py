import numpy as np
import csv

lookup_list = np.genfromtxt('formB_int.csv', delimiter=',', skip_header=1)

voltage_list = lookup_list[0,:]
temp_current = lookup_list[:,0:2]
soc_list = lookup_list[:,2:]


for j in range(len(soc_list[:,1])):
    for i in range(len(soc_list[j,:-1])):
        if str(soc_list[j,i]) == 'nan':
            soc_list[j,i]= 0
        if i != len(soc_list[j,:]):
            if str(soc_list[j,i+1]) == 'nan':
                soc_list[j,i+1]=soc_list[j,i]

lookup_list = np.column_stack((temp_current, soc_list))
print(lookup_list)

with open('formB.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(["Temp", "Curr", "Voltages"])
    writer.writerow(voltage_list)
    np.savetxt('formB.csv', lookup_list, delimiter=",",fmt='%10.5f')
