#---------------------------------------------------------------------------------
# Filename:        myFunctions.py
# Project name:    Pi-CAN-Analyzer
# Author:          Hannes Bohnengel
# Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
# Last modified:   11 Sep 2017
# Comments:        This file contains some custom functions 
#---------------------------------------------------------------------------------

# To support the new print function of python3
from __future__ import print_function
import struct # for float conversion

# Callback function how to react when receiving a message from a socket
def printResponse(args):
	print("Received: {}".format(args))

# This function checks if the entries of a list are valid hex characters (0-9, a-f, A-F)
# and returns 0 if all characters are valid and -1 if one or more is invalid
def checkHex(data):
	result = 0
	for i in data:
		if ((i >= 48 and i <= 57) or (i >= 97 and i <= 102) or (i >= 65 and i <= 70)) != True:
			print("Invalid character: %c" % i)
			result = -1
	return result

# This function converts a list of 16 hex values into a list of 8 bytes
# and returns this new list or -1 if something went wrong
# Example: [0, 0, 1, 0, 0, A, A, D, 5, 8, 9, 1, F, E, A, 3]
#       -> [00,10,0A,AD,58,91,FE,A3] 
def hex2byte(hexval):
	byte = [0,0,0,0,0,0,0,0]
	for i in range(0, 8):
		byte[7-i] = hexval[16 - (i*2+1)] + (hexval[16 - (i*2+2)] << 4)
	return byte

# This function converts the user input to a list consisting of single ascii values
def unicode2ascii(data):
	return map(ord, data)

# This function converts ascii symbols which are received as a string to a list with the
# respective hexadecimal value (e.g. "AB12" -> [0xA, 0xB, 0x1, 0x2])
def ascii2hex(data):
    
	# Check if data length is valid (1 - 16 hex values)
	if len(data) == 0 or len(data) > 16:
		print("Invalid length!")
		return -1

	# Check if input contains invalid values
	tmp = checkHex(data)
	if tmp == -1:
		return -1

	# Message to be sent
	msg = [0,0,0,0,0,0,0,0]

	# Temporary buffer
	tmp = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

	# Convert from ASCII to decimal value (e.g. 'A' -> 10)
	for i in range(0, len(data)):
		if data[i] >=48 and data[i] <= 57:
			# 0-9
			tmp[i] = data[i] - 48
		elif data[i] >= 97 and data[i] <= 102:
			# a-f
			tmp[i] = data[i] - 87
		elif data[i] >= 65 and data[i] <= 70:
			# A-F
			tmp[i] = data[i] - 55

	# Shift content to right side of array (e.g. [A, 1, F, 5, 0, 0, 0] -> [0, 0, 0, A, 1, F, 5]
	# if not all 8 bytes are used
	if len(data) < 16:
		for i in range(0, len(data)):
			tmp[15-i] = tmp[len(data)-1-i]
			tmp[len(data)-1-i] = 0

	# Group two hex values to one byte
	msg = hex2byte(tmp)

	return msg

# Print a list storing hexadecimal values
def printHex(hexlist):
	for i in hexlist:
		print(" %02X" % i, end= "")

# This function converts four bytes to a single precision float (IEEE 754)
# Example:
# bytelist = [0xC0, 0x68, 0x5F, 0x40]
# binary representation:
#
#   sign bit    exponent 30-23 (8 bits)       fraction 22-0 (23 bits)
#       \      /        ______________/      /    __________________/
#        \    /        / ___________________/    /
#         \  /        / /                       /
#         1 100 0000 0 1101000 01011111 01000000
#         \        / \       / \      / \      /
#         |       /  |      /  |     /  |     /
# bytes:   0xC0__/    0x68_/    0x5F/    0x40/

# output:
# print("{}".formatbyte2float(bytelist) = 3.49076843262

def byte2float(bytelist):
	# create a string out of the bytelist, since unpack needs that as an input
	result = ''.join(chr(i) for i in bytelist)
	# unpack would return a tuple with only one element -> use [0]-operator
	# the "<"-symbol denotes the byte order
	return struct.unpack('<f', result)[0]
    








