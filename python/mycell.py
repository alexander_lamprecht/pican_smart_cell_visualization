#---------------------------------------------------------------------------------
# Filename:        mycell.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisor:      Alexander Lamprecht
# Last modified:   24 Aug 2018
# Comments:        This script defines class cell which stores cell's data like
#                  temperature, current, voltage and soc of each cell as object
#---------------------------------------------------------------------------------


# import numpy as np
from mySoCCalc import *
import threading

class Cell():
    """Basic cell class"""
    num_of_cells = 0

    cell_id = 0
    temperature = 0
    current = 0
    voltage = 3.3
    capacity = 1.1
    soc = 0
    target_soc = 100
    soc_grad_min = 0
    soc_grad = 0
    soc_fac = 10 ## adjusting the maximum soc change speed
    I_bal = 1
    init = False
    log = {}

    def __init__(self, temperature=297, current=0, voltage=3.3):
        self.cell_id = Cell.num_of_cells
        Cell.num_of_cells += 1
        self.temperature = temperature
        self.current = current
        self.voltage = voltage
        self.soc_grad_min = self.soc_fac*self.capacity/3600
        self.soc_grad = self.soc_grad_min


    def set_temperature(self, temp):
        """sets the object's variable for temperature"""
        self.temperature = temp

    def set_current(self, curr):
        """sets the object's variable for current"""
        self.current = curr
        self.soc_grad = max(self.soc_grad_min, self.soc_fac*self.capacity*self.current/3600)
        self.compute_soc()

    def set_voltage(self, volt):
        """sets the object's variable for voltage"""
        self.voltage = volt

    def get_voltage(self):
        """returns the object's variable for voltage"""
        return self.voltage

    def get_temperature(self):
        """returns the object's variable for temperature"""
        return self.temperature

    def get_current(self):
        """returns the object's variable for current"""
        return self.current

    def get_soc(self):
        """returns the object's variable for soc"""
        return self.soc

    def get_id(self):
        """returns the object's id """
        return self.cell_id

    # def init_soc(self,temp,curr,volt):
    #     """initializes the object's soc """
    #     return calc_soc(temp, curr, volt)

    def compute_soc(self):
        """computed the current target soc"""
        if not self.init:
            self.soc = calc_soc(self.temperature, self.current, self.voltage)
            self.init = True
        self.target_soc = calc_soc(self.temperature, self.current, self.voltage)
        self.update_soc()

# SOH calculation not implemented yet
    def compute_soh(self, temp, current, volt):
        """computes the current soh"""
        pass

    def update_soc(self):
        """updates the object's soc to slowly approach the target soc"""
        if abs(self.soc - self.target_soc) > 10*self.soc_grad_min:
            self.soc -= np.sign(self.soc - self.target_soc) * self.soc_grad
  
    def append_data(self):
        """adds data to the object's log"""
        self.log[str(datetime.datetime.now()).split('.')[0]] = (self.temperature, self.current, self.voltage, self.soc)

# returns logged data in json format sort wrt time, can be used to send over socket.
    def jsonlog(self):
        """returns the object's log"""
        json_log = json.dumps(self.log, sort_keys=True)
        return json_log

    def start_soc_timer(self):
        """starts the soc update timer"""
        