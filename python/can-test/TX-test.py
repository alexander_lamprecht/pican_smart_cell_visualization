
import can

data = [0, 25, 0, 1, 3, 1, 4, 1]
id = 0x7de

# create a bus instance (configured in ~/.can)
bus = can.interface.Bus(channel='can0', bustype='socketcan')

# create a notifier which prints received messages to the terminal
msg = can.Message(arbitration_id=id, data=data, extended_id=False)

print("--------------------------------------------------------------------")
print("Sending Message:")
print("ID:   %x" %(id))
#print("ID:   {}".format(id))
#print("Data: {}".format(data))
string = str.join("", ("%02X " % i for i in data))
print("Data: %s" %(string))

bus.send(msg)

print("--------------------------------------------------------------------")

