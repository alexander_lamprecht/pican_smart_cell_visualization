#---------------------------------------------------------------------------------
# Filename:        balance_test.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisor:      Alexander Lamprecht
# Last modified:   08 Sep 2018
# Comments:        This script enables/disables balancing in smart cell demonstrator
#---------------------------------------------------------------------------------

import can

# create a bus instance (configured in ~/.can)
bus=can.interface.Bus(channel='can0', bustype='socketcan')

# create a notifier which prints received messages to the terminal
msg = can.Message(arbitration_id=0x0030ff00, data=[1], extended_id=True) # put data as [1] to enable balancing and [0] to disable

bus.send(msg)
print("--------------------------------------------------------------------")
print(msg)
print("--------------------------------------------------------------------")