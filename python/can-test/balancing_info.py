#---------------------------------------------------------------------------------
# Filename:        balance_info.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisor:      Alexander Lamprecht
# Last modified:   08 Sep 2018
# Comments:        This script give information of cell involved in balancing in
#                  smart cell demonstrator
#---------------------------------------------------------------------------------

#to support print function from python3
from __future__ import print_function

#to import scripts in parent directory
import sys
sys.path.insert(0, '../')

from myFunctions import *
from myCAN import *
from myMessage import *


# Setup can communication
channel = 'can0'
bustype = 'socketcan'
ID = 0x7de
myCANbus = myCAN(channel, bustype, ID)

# Check if CAN interface is already brought up and if yes set it up
if myCANbus.check() == 1:
    myCANbus.up = True
else:
    myCANbus.up = False

if myCANbus.up:
    while True:
        CANmsg = myCANbus.bus.recv()
        if (CANmsg != None):
            origin = (CANmsg.arbitration_id) & 0xFF
            target = (CANmsg.arbitration_id >> 8) & 0xFF
            msgType = (CANmsg.arbitration_id >> 16) & 0xFFFF
            
            if(msgType == MSG_TYPE.SEND_ACK):
                print("charging from cell:",origin,"to cell:",target,"started")

            if(msgType == MSG_TYPE.UNBLOCK):
                my_data= list(CANmsg.data)
                print("charging from cell:",my_data[0],"to cell:",my_data[1],"stoppped")

else:
    print("Bring CAN up first")
