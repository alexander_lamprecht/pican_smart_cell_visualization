
import can

# create a bus instance (configured in ~/.can)
bus = can.interface.Bus(channel='can0', bustype='socketcan')

# create a notifier which prints received messages to the terminal
notifier = can.Notifier(bus, [can.Printer()])

print("--------------------------------------------------------------------\n")
print("Listening for messages. Press any key to exit.\n")
print("Time stamp               Addr.   Type   Len. Data\n")

raw_input("")

print("--------------------------------------------------------------------")
