#---------------------------------------------------------------------------------
# Filename:        mySoCCalc.py
# Project name:    Internet of Things of smart decentralised cells
# Author:          Satyam Gaba
# Supervisor:      Alexander Lamprecht
# Last modified:   16 Aug 2018
# Comments:        This script calculates the soc value of the cell corresponding
#                  to a given temp, load current and voltage.
#---------------------------------------------------------------------------------




#import SoC lookup table
mylookuptable = 'lookup_table.csv'
import numpy as np
from scipy import interpolate


# this function finds the index on the element in a sort list(in ascending or descending order) by linear search.
def find_index(element,list,condition):
    index=0
    if condition == 'descend':
        for i in range((len(list)-1)):
            if (element < list[i]) and (element >= list[i+1]):
                index = i
                break
            elif element < list[-1]:
                index = len(list)-1
    elif condition == 'ascend':
        for i in range((len(list)-1)):
            if (element >= list[i]) and (element < list[i + 1]):
                index = i
                break
            elif element > list[-1]:
                index = len(list)-1
    return index

# this function calculates the soc value of the cell corresponding to a given temp, load current and voltage.
# It approximates the live voltage value of the cell to the nearest voltage value in the lookup table and
# uses linear interpolation for in between values of current and temperature.

def calc_soc(temp,current,voltage):

    pos_current = True
    
    # manipulation for negative current
    if current < 0:
        current = abs(current)
        pos_current = False

    def soc_interp(temp,current,voltage):
        M = np.genfromtxt(mylookuptable, delimiter=',', dtype=float, skip_header=1)

        temp_list = np.unique(M[1:None, 0])
        current_list = np.unique(M[1:None, 1])
        voltage_list = M[0, 2:None]
        voltage_list[::-1].sort()

        lookup = np.zeros((len(voltage_list), len(current_list), len(temp_list)))
        for i in range(len(M[:, 0])):
            for j in range(len(temp_list)):
                if M[i, 0] == temp_list[j]:
                    for k in range(len(current_list)):
                        if M[i, 1] == current_list[k]:
                            lookup[:, k, j] = M[i, 2:None]


        idx_temp = find_index(temp, temp_list, 'ascend')
        idx_current = find_index(current, current_list, 'ascend')
        idx_voltage = find_index(voltage, voltage_list, 'descend')

        the_cell = lookup[idx_voltage:idx_voltage + 2, idx_current:idx_current + 2, idx_temp:idx_temp + 2]

        num_vol_val = np.shape(the_cell[0])
        num_curr_val = np.shape(the_cell)[1]
        num_temp_val = np.shape(the_cell)[2]

        try:
            if (num_vol_val == 2 or voltage >= voltage_list[-1]): # if given voltage is not less than the minimum value in lookup table
                # if temperature in interpolation range
                if num_temp_val == 2:
                    # current in interpolation range(i.e. less than the maximum value in lookup_table.csv)
                    if num_curr_val == 2:
                        if abs(voltage - voltage_list[idx_voltage]) < abs(voltage - voltage_list[idx_voltage + 1]):
                            the_tile = the_cell[0, :, :]
                            the_tile = np.reshape(the_tile, (2, 2, 1))
                        else:
                            the_tile = the_cell[1, :, :]
                            the_tile = np.reshape(the_tile, (2, 2, 1))
                        f = interpolate.interp2d(current_list[idx_current:idx_current + 2], temp_list[idx_temp:idx_temp + 2],
                                                the_tile)
                        vq = f(current, temp)
                        soc = round(float(vq), 1)
                    else:  # current outside interoplation range but temp in interpolation range
                        the_tile = the_cell.reshape((2, 2))
                        if abs(voltage - voltage_list[idx_voltage]) < abs(voltage - voltage_list[idx_voltage + 1]):
                            the_segment = the_tile[0, :]
                        else:
                            the_segment = the_tile[1, :]
                        f = interpolate.interp1d(temp_list[idx_temp:idx_temp + 2], the_segment)
                        vq = f(temp)
                        soc = round(float(vq), 1)

                else:  # only one temperature in lookup table or given temperature outside interpolation range
                    if num_curr_val == 2:  # current in interpolation range
                        the_tile = the_cell.reshape((2, 2))
                        if abs(voltage - voltage_list[idx_voltage]) < abs(voltage - voltage_list[idx_voltage + 1]):
                            the_segment = the_tile[0, :]
                        else:
                            the_segment = the_tile[1, :]
                        f = interpolate.interp1d(current_list[idx_current:idx_current + 2], the_segment)
                        vq = f(current)
                        soc = round(float(vq), 1)
                    else:  # current above interpolation range
                        the_segment = the_cell.reshape((2))
                        # approximate voltage to the closest voltage value in the table
                        if abs(voltage - voltage_list[idx_voltage]) < abs(voltage - voltage_list[idx_voltage + 1]):
                            soc = the_segment[0]
                        else:
                            soc = the_segment[1]
            else: #voltage less than voltage list in lookup table
                if num_temp_val == 2:
                    if num_curr_val == 2:
                        the_tile = the_cell.reshape((2, 2))
                        f = interpolate.interp2d(current_list[idx_current:idx_current + 2], temp_list[idx_temp:idx_temp + 2],
                                            the_tile)
                        vq = f(current, temp)
                        soc = round(float(vq), 1)
                    else:
                        the_segment = the_cell.reshape((2))
                        f = interpolate.interp1d(temp_list[idx_temp:idx_temp + 2], the_segment)
                        vq = f(temp)
                        soc = round(float(vq), 1)

                else: #one temp
                    if num_curr_val == 2:
                        the_segment = the_cell.reshape((2))
                        f = interpolate.interp1d(current_list[idx_current:idx_current + 2], the_segment)
                        vq = f(current)
                        soc = round(float(vq), 1)
                    else: # one current
                        the_element = the_cell.reshape(1)
                        soc = round(float(the_element), 1)
        except:
            print("some exception occurred")
            soc = np.nan

        return soc

    if pos_current == True:
        soc = soc_interp(temp, current, voltage)
    else:
        soc_0 = soc_interp(temp, 0, voltage)
        soc_pos = soc_interp(temp, current, voltage)
        soc = soc_0-abs(soc_pos-soc_0)
    return soc
