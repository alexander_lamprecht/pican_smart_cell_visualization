
#---------------------------------------------------------------------------------
# Filename:        myMessage.py
# Project name:    Pi-CAN-Analyzer
# Author:          Hannes Bohnengel
# Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
# Last modified:   08 Sep 2017
# Comments:        This file contains the message type
#---------------------------------------------------------------------------------

from enum import Enum

class MSG_TYPE(Enum):
	NONE = 0x0000
	UNBLOCK = 0x0001
	BLOCK = 0x0002
	STATUS_RESPONSE = 0x0003
	SOC = 0x0010
	VOLTAGE = 0x0011
	TEMPERATURE = 0x0012
	SEND_REQ = 0x0020
	SEND_ACK = 0x0021
	EXT_SEND_REQ = 0x0022
	EXT_SEND_ACK = 0x0023
	SET_BALANCING = 0x0030
	SET_STRATEGY = 0x0031
	SET_MONITOR = 0x0032
	SET_MONITOR_BOUND = 0x0033
	SET_BC_SETTINGS = 0x0034
	SET_BC_PERIOD = 0x0035
	SET_BC_BOUND = 0x0036
	SET_OCV_BALANCING = 0x0037
	SET_OCV_LATENCY = 0x0038
	SET_U_TIMER = 0x0039
	SET_U_TIMER_VALUES = 0x0040
	SET_I_TIMER = 0x0041
	SET_I_TIMER_VALUES = 0x0042
	SET_KF_TIMER = 0x0043
	SET_KF_TIMER_VALUES = 0x0044
	SET_PWM_PARAMS = 0x0050
	AUTODETECT_RANDOM = 0x00A0
	AUTODETECT_PULSE = 0x00A1
	AUTODETECT_ASSIGN = 0x00A2
	DEBUG_SOC = 0x0D00
	DEBUG_VOLTAGE = 0x0D01
	DEBUG_SET_SCREEN = 0x0D10
	DEBUG_SET_STATUS = 0x0D11
	DEBUG_SET_SOC = 0x0D12
	DEBUG_SET_VOLTAGE = 0x0D13
	DEBUG_SET_CURRENT = 0x0D14
	DEBUG_SET_ID = 0x0D15
	DEBUG_FORCE_SEND = 0x0D16
	UNDEFINED = 0x1FFF
#	CURRENT = xxxx 

def getExtID(msgtype = 'NONE', origin = 0x00, target = 0x00):
	"""
	Build an extended (29-bit) identifier with the given properties

	Args:
		  idType: message type
		  origin: ID of sender ECU (0x00 - 0xFF)
		  target: ID of target ECU (0x00 - 0xFE) [0xFF is broadcast-ID]
	"""

	# Check if msgtype is valid
	if(True):
		return (msgtype<<16) | (target<<8) | (origin)
	else:
		return 0x00000000

def getMsgTypeString(argument):

	switcher = {
		0x0000: "NONE",
		0x0001: "UNBLOCK",
		0x0002: "BLOCK",
		0x0003: "STATUS_RESPONSE",
		0x0010: "SOC",
		0x0011: "VOLTAGE",
		0x0012: "TEMPERATURE",
		0x0020: "SEND_REQ",
		0x0021: "SEND_ACK",
		0x0022: "EXT_SEND_REQ",
		0x0023: "EXT_SEND_ACK",
		0x0030: "SET_BALANCING",
		0x0031: "SET_STRATEGY",
		0x0032: "SET_MONITOR",
		0x0033: "SET_MONITOR_BOUND",
		0x0034: "SET_BC_SETTINGS",
		0x0035: "SET_BC_PERIOD",
		0x0036: "SET_BC_BOUND",
		0x0037: "SET_OCV_BALANCING",
		0x0038: "SET_OCV_LATENCY",
		0x0039: "SET_U_TIMER",
		0x0040: "SET_U_TIMER_VALUES",
		0x0041: "SET_I_TIMER",
		0x0042: "SET_I_TIMER_VALUES",
		0x0043: "SET_KF_TIMER",
		0x0044: "SET_KF_TIMER_VALUES",
		0x0050: "SET_PWM_PARAMS",
		0x00A0: "AUTODETECT_RANDOM",
		0x00A1: "AUTODETECT_PULSE",
		0x00A2: "AUTODETECT_ASSIGN",
		0x0D00: "DEBUG_SOC",
		0x0D01: "DEBUG_VOLTAGE",
		0x0D10: "DEBUG_SET_SCREEN",
		0x0D11: "DEBUG_SET_STATUS",
		0x0D12: "DEBUG_SET_SOC",
		0x0D13: "DEBUG_SET_VOLTAGE",
		0x0D14: "DEBUG_SET_CURRENT",
		0x0D15: "DEBUG_SET_ID",
		0x0D16: "DEBUG_FORCE_SEND",
		0x1FFF: "UNDEFINED"
	}
	return switcher.get(argument, "NOT EXISTING")

