# What is in here?

This folder contains different files associated to this project.

Folder/File | Description
------ | ------
[`PICAN2_Schematics_RevB.pdf`](PICAN2_Schematics_RevB.pdf) | Schematics of the PICAN2 add-on board
[`PICAN2_UserGuide_RefB_V1.2.pdf`](PICAN2_UserGuide_RefB_V1.2.pdf) | User guide of the PICAN2 add-on board
[`CellDischargeTrim.csv`](CellDischargeTrim.csv) | This file contains the original discharge voltage measurement of a single cell. The mapping is timestamp to voltage, beginning from 100% and ending at 0%.
[`VoltageToSoC.csv`](VoltageToSoC.csv) | This file contains a mapping from voltage to state of charge, based on [`CellDischargeTrim.csv`](CellDischargeTrim.csv).
[`VoltageToSoC.xlsx`](VoltageToSoC.xlsx) | This file contains the calculation between timestamp and respective state of charge as well as a plot which shows the voltage characteristics of a cell.
[`GitHowTo.txt`](GitHowTo.txt) | This file contains a short guide how to use git in the command line.
[`Final_Presentation.pptx`](Final_Presentation.pptx) | This is the final presentation held on the 10 October 2017.
[`Final_Presentation_Handout.pptx`](Final_Presentation_Handout.pptx) | This is the handout version of the final presentation held on the 10 October 2017.
[`Final_Presentation_Handout.pdf`](Final_Presentation_Handout.pdf) | This is the PDF handout version of the final presentation held on the 10 October 2017.
