
/*---------------------------------------------------------------------------------
	Filename:        myGUI.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   19 Sep 2017
	Comments:        This is a javascript file containing all GUI related code.
---------------------------------------------------------------------------------*/

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Parameter to be changed via the settings GUI

// Some parameters for the drawing
var strokeWidth = 4;
var strokeColor = "#191919";
var centeredLabels = false;

// Update intervall of the values of the plot (in ms)
var plotInterval = 300;

// Timeout (in ms) for displaying the voltage
var voltageTimeout = 10000;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// The following two are different possibilities to use the GUI for
// They need just to be added via gui.addGlobals("dropdown", "label");

// Create a dropdown list
// var dropdown = ["item-1", "item-2", "item-3"];

// Create a text entry
// var label = "label";

// Graphical User Interface object
var gui;
var visible = false;

// Some position references
var xRef = 10;
var yRef = 10;
var xSpace = 10;
var ySpace = 30;

// DOM elements
var refreshButton, homeButton, check1, infoText;

// Location of the TUMCREATE logo
var logoPath = "../src/logo.jpg";

// Object representing the logo image
var logoImg;

// Create GUI for settings (the sliderRange is valid for all following parameters until redefined)
function setupGUI() {
  gui = createGui("Settings", windowWidth - 220, 50);
  colorMode(HSB);
	sliderRange(50,2000,50);
  gui.addGlobals('plotInterval');
	sliderRange(1000,15000,100);
  gui.addGlobals('voltageTimeout');
  sliderRange(1, 5, 0.1);
  gui.addGlobals("centeredLabels", "strokeColor", "strokeWidth");

	// Check if visible is enabled by default
	if(visible) gui.show(); else gui.hide();
}

// Create buttons
function createDOMelements() {
	// Create the home button to return to landing page
	homeButton = createButton("Home");
  homeButton.position(xRef, yRef);
  homeButton.mousePressed(homePage);

	// Create the refresh button (alternative to [F5])
	refreshButton = createButton("Refresh");
  refreshButton.position(homeButton.x + homeButton.width + xSpace, yRef);
  refreshButton.mousePressed(reloadPage);

	// Create the header
	header = createElement("h2", "Smart Cell Demonstrator");
	header.position(windowWidth/2 - (header.size()).width/2, (2*yRef));

	// This info text contains also a link which enables touch interface or just clicking on the [S]
	infoText = createP('Press <a href="javascript:toggleGUI();">[S]</a> to show/hide the settings.');
	infoText.position(xRef, homeButton.y + ySpace);
	//infoText.fontSize("25px");
	infoText.style("font-size", "12px")
}

// This function loads the logo
function loadLogo() {
  logoImg = loadImage(logoPath);
}

function drawLogo() {
  image(logoImg, (width - logoImg.width/15 - 2*xSpace), yRef/2, logoImg.width/15, logoImg.height/15);
}

// Check for keyboard events
function keyPressed() {
	// Check which key is pressed
	switch(key) {
		// If 'S' is hit
		case "S":
			toggleGUI();
		  break;
		// insert here different additional key interactions with new case statements
		/* ---------------------
		case 'X':
		... code here ...
		break; 
		--------------------- */
	}
	// Prevent any default behavior (dependent of the browser) ===> commands like [F5] for refresh don't work anymore
	//return false;
}

// This function toggles the visability of the settings GUI
function toggleGUI() {
	visible = !visible;
	if(visible) gui.show(); else gui.hide();
}

// This function is called when the home button is pressed
function homePage() {
	window.location.pathname = "../";
}

// This function is called when the refresh button is pressed
function reloadPage() {
  location.reload();
}

// Dynamically adjust the canvas size
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
	repositionDOMelements();
}

// This function repositions the DOM elements
function repositionDOMelements() {
	// Adjust the position of DOM elements
	header.position(width/2 - (header.size()).width/2, (2*yRef));
	infoText.position(xRef, homeButton.y + ySpace/2);
}
