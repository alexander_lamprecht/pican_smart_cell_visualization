
/*---------------------------------------------------------------------------------
	Filename:        myQuery.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   18 Sep 2017
	Comments:        This is a javascript file containing some functions to query
                   a comma separated CSV file for Voltage to SoC mapping.
                   The file has a header for each column.
---------------------------------------------------------------------------------*/

// Variable to store information of the CSV table
var table, rows, columns, maxVoltage, minVoltage;

// Location of the CSV file
var csvPath = "../src/VoltageToSoC.csv";

// Variable to store the table as 2D array
var myArray;

function preload() {
	// Load the table (with headers) and define callback functions 
  table = loadTable(csvPath, "csv", "header", callBackSuccess, callBackError);
}

function callBackSuccess() {
	console.log("CSV table for SoC mapping loaded");
	rows = table.getRowCount();
	columns = table.getColumnCount();
	maxVoltage = parseFloat(table.getString(0,0));
	minVoltage = parseFloat(table.getString((rows-1),0));

	// DEBUG
	//console.log("First pair: (" + table.getString(0,0) + "," + table.getString(0,1) + ")");
	//console.log("MinVol = " + minVoltage + "\tMaxVol = " + maxVoltage);

	// Allocate an array of the right size (here with strings instead of float values)
	myArray = table.getArray();

	// Convert the string elements to float values
	readCSV();
}

function callBackError() {
	console.log("Could not load CSV table for SoC mapping\nSoC values will not be displayed correctly");
}

function readCSV() {
	// Cycle through the table and extract data as 
  for (var r = 0; r < rows; r++) {
    for (var c = 0; c < columns; c++) {
			myArray[r][c] = parseFloat(myArray[r][c]);
    }
	}
}

function printArray(array) {
	console.log("Printing Array:");
	for (var r = 0; r < rows; r++) {
    for (var c = 0; c < columns; c++) {
			console.log("array[" + r + "," + c + "]: " + array[r][c]);
    }
	}
}


function getSoC(voltage) {
	var result;
	// Check if given voltage is in the range of the CSV table
	if (voltage > maxVoltage) {
		//console.log("Voltage out of range of CSV file (larger)\nReturning SoC = 100%");
		result = 1.0;
	}
	else if (voltage < minVoltage && voltage >= 0.0) {
		//console.log("Voltage out of range of CSV file (smaller)\nReturning SoC = 0%");
		result = 0.0;
	}
	else if (voltage < 0.0) {
		// return -1 as error code
		console.log("Voltage has a negative sign\nReturning -1 as error");
		result = -1;
	}
	else {
		// Cycle through table until the given voltage is found
		for (var r = 0; r < rows; r++) {
			if(voltage >= myArray[r][0]) {
				console.log(myArray[r][0]);
				break;
			}
		}
		//console.log("SoC(" + myArray[r][0] + ") = " + myArray[r][1]);
		result = myArray[r][1];
	}
	
	return result;
}


function getValue(reference, type) {
	var result;
	// check which value to fetch from the table

	if (typeof(type) != "string") {
		console.log("Please provide a string as type to the function getValue(reference, type)");
		console.log("Returing -1 as error");
		return -1;
	}

	switch(type) {
		// Fetching the SoC with given voltage
		case "SoC":
			// Check if given voltage is in the range of the CSV table
			if (reference > maxVoltage) {
				//console.log("Given voltage out of range of CSV file (larger)\nReturning SoC = 100%");
				result = 1.0;
			}
			else if (reference < minVoltage && reference >= 0.0) {
				//console.log("Given voltage out of range of CSV file (smaller)\nReturning SoC = 0%");
				result = 0.0;
			}
			else if (reference < 0.0) {
				// return -1 as error code
				console.log("Given voltage has a negative sign\nReturning -1 as error");
				result = -1;
			}
			else {
				// Cycle through table until the given voltage is found
				for (var r = 0; r < rows; r++) {
					if(reference >= myArray[r][0]) {
						//console.log("SoC(" + reference + " V) = " + myArray[r][1] + " %");
						break;
					}
				}
				//console.log("SoC(" + myArray[r][0] + " V) = " + myArray[r][1] + " %");
				// Choosing the respective value and dividing it by 100 (100% = 1.0)
				result = myArray[r][1]/100;
			}
		break;
		// Fetching the voltage with given SoC
		case "Voltage":
			if (reference > 100.0 || reference < 0.0) {
				console.log("Given SoC is out of range\nReturning -1 as error");
				result = -1;
			}
			else {
				// Cycle through table until the given SoC is found
				for (var r = 0; r < rows; r++) {
					if(reference >= myArray[r][1]) {
						//console.log("Voltage(" + reference + " %) = " + myArray[r][0] + " V");
						break;
					}
				//console.log("Voltage(" + myArray[r][1] + " %) = " + myArray[r][0] + " V");
				// Choosing the respective value
				result = myArray[r][0];
				}
			}
		break;
		// Default case
		default:
		console.log("Wrong type for function getValue(reference, type)");
	}
	return result;
}
