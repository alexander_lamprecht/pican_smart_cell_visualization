
/*---------------------------------------------------------------------------------
	Filename:        myCell.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   26 Sep 2017
	Comments:        This is a javascript file containing an object describing the
                   status of a cell and visualizing it.
---------------------------------------------------------------------------------*/

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Don't change these values, otherwise the cells might overlap and 
// the sizes jump at certain width/height ratios

// The scaling factor of the size of the battery
var cellScale = (1/3);
// The ratio between cell x and y size (xSize = cellxyRatio * ySize)
var cellxyRatio = 0.5;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Cell objects
var numCells = 5;
var batteries = [];

// Colors for different state of charges
var full   = '#00FF00';
var medium = '#FFA000';
var empty  = '#FF0000';

// Thresholds for state of charges (three areas)
var thres = [0.2, 0.55]

// The length of the plot
var lengthArray = 40;

// Cell object
function myCell(ID) {
	// Variables for drawing
	this.xPos = 0;
	this.yPos = 0;
	this.xSize = 50;
	this.ySize = 100;

	// Status values
	this.ID = ID;
	this.soc = 0;
	this.voltage = 0.0;

	// Array containing vector values for ploting the voltage
	this.voltageArray = [];

	// This array holds voltage values (not in vector form)
	this.tempArray = [];

	// A timestamp for the last plotted voltage value
	this.plotTimestamp = new Date();

	// A timestamp for the last voltage value received from socket
	this.voltageTimestamp = new Date();
	this.voltageUptodate = false;

	// Create some DOM elements for displaying the status values
	var myDiv = createDiv('');
	myDiv.style("float", "right");
	myDiv.style("color", "black");
	myDiv.style("font-size", "9pt");
	myDiv.style("font-weight", "900");

	this.idText = createP("ID: " + this.ID);
	this.idText.parent(myDiv);

	this.volText = createP("Voltage: " + this.voltage + " V");
	this.volText.parent(myDiv);

	this.socText = createP("SOC: " + this.soc + " %");
	this.socText.parent(myDiv);

	// This function draws each cell as a battery symbol
	// and is called in a loop.
	this.display = function(strokeColor, strokeWidth) {
		// Set the fill color according to the SOC		
		var col;
		if (this.soc >= thres[1]) {
			col = full;
		}
		else if (this.soc < thres[1] && this.soc >= thres[0]) {
			col = medium;
		}
		else {
			col = empty;
		}

		push();		// Start a new drawing state
		// Filling representing the charge
		fill(col);
		noStroke();
		rect(this.xPos, this.yPos + this.ySize - this.ySize*this.soc, this.xSize, this.ySize * this.soc);

		// Check if this cell is not up to date any more
		if (this.voltageUptodate != true) {
			strokeColor = '#AAAAAA';
			this.soc = 0.0;
			this.voltage = 0.0;
		}/*
		else {
			strokeColor = "#191919";
		}*/

		// Body of the hole cell
		noFill()
		stroke(strokeColor);
		strokeWeight(strokeWidth);
		rect(this.xPos, this.yPos, this.xSize, this.ySize);

		// Anode (so it looks like a battery)
		fill(strokeColor);
		rect(this.xPos + (this.xSize - this.xSize*0.25)/2, this.yPos - this.ySize/25, this.xSize * 0.25, this.ySize/25);
		pop();	// Restore original state

		// Update the values of the labels
		this.idText.html("Cell ID: 0x0" + this.ID);
		this.volText.html("Voltage: " + this.voltage + " V");
		this.socText.html("SOC: " + (this.soc * 100).round(0) + " %");

		// Reposition the labels if window is moved or size of battery is changed
		if (centeredLabels) {
			this.idText.position(this.xPos + this.xSize/2 - this.idText.size().width/2, this.yPos + this.ySize);
			this.volText.position(this.xPos + this.xSize/2 - this.volText.size().width/2, this.idText.y + this.idText.height);
			this.socText.position(this.xPos + this.xSize/2 - this.socText.size().width/2, this.volText.y + this.volText.height);
		}
		else {
			this.idText.position(this.xPos + this.xSize/20, this.yPos + this.ySize);
			this.volText.position(this.xPos + this.xSize/20, this.idText.y + this.idText.height);
			this.socText.position(this.xPos + this.xSize/20, this.volText.y + this.volText.height);
		}

		// Draw the plot
		drawPlot(this.voltageArray, this.xPos, this.yPos + this.ySize + this.idText.height + this.volText.height, this.xSize/1.2, this.xSize/1.5);
		
	}
	// This function initializes the socket connection
	this.initSocket = function() {
		// Listen to the socket for status updates
		listenSocket("vol", setVoltage);
	}
	// This function initializes an array with points
	this.initPlot = function() {
		for(var i = 0; i < lengthArray; i++) {
			this.voltageArray[i] = new GPoint(i, 1);

			// TESTING
			this.tempArray[i] = 3.0;
			// TESTING

		}
	}
	this.updatePlot = function() {
		for (var i = 0; i < this.tempArray.length; i++) {
			this.voltageArray[i].setY(this.tempArray[i]);
			this.voltageArray[i].setX(i);
		}
	}
}

// This function is called when a voltage value is received.
// If the ID in the message frame exists, the value is assigned to the cell
// The received data is a list consisting of:
// data[0] contains the origin ID (e.g. 0x01)
// data[1] contains the voltage value as float (e.g. 3.40939521789551)
function setVoltage(data) {
	// Check if ID exists and assign the voltage value to the respective cell
	if(typeof batteries[data[0]] === 'undefined') {
		console.log("ID " + data[0] + " does not exist!");
	}
	else {
		// Set the cells voltage
		batteries[data[0]].voltage = data[1].round(3);
		
		// Set the respective SoC value (according the voltage - SoC mapping in a given CSV file)
		batteries[data[0]].soc = getValue(batteries[data[0]].voltage, "SoC");

		if(batteries[data[0]].soc == -1) {
			console.log("ValueError while fetching SoC");
			batteries[data[0]].soc = 0.0;
		}

		// Create a timestamp for the current moment
		var now = new Date();

		// Check when the last voltage value was pushed into the plot array
		if(now.getTime() - batteries[data[0]].plotTimestamp.getTime() > plotInterval) {
			// Push the new value into the array and delete the oldest one
			updateArray(batteries[data[0]].tempArray, batteries[data[0]].voltage.round(2));
			batteries[data[0]].plotTimestamp = new Date();
		}

		// set timestamp when last values are updated
		batteries[data[0]].voltageTimestamp = new Date();
		batteries[data[0]].voltageUpdtodate = true;
	}
}

// This function creates new cells
function initCells() {
	for(var i = 0; i < numCells; i++) {
		// Create cell objects and assign each one an ID
		batteries.push(new myCell(i));
		// Initialize Socket
		//batteries[i].initSocket();

		// set up plots
		batteries[i].initPlot();
	}

	// Listen to the socket for status updates
	listenSocket("vol", setVoltage);

	// Call checkFreshness function every ... ms
	setInterval(checkFreshness, 500);

	// Call updatePlots function every ... ms 
	setInterval(updatePlots, 200);
}

// This function updates the plot for all cells
function updatePlots() {
	for(var i = 0; i < numCells; i++) {
		batteries[i].updatePlot();
	}
}

// This function draws all cells
function drawCells() {
	for(var i = 0; i < numCells; i++) {

		// Redraw the cell
		batteries[i].display(strokeColor, strokeWidth);

		// height*cellScale*cellxyRatio = the supposed to be xSize of a cell
		if ((height*cellScale*cellxyRatio) < width/(numCells+1)) { 
			// Set the size of the cells
			batteries[i].ySize = height/3;
			batteries[i].xSize = batteries[i].ySize/2;
		}
		else {
			batteries[i].xSize = width/(numCells+1);
			batteries[i].ySize = batteries[i].xSize * 2;
		}

		// Position the cells in a row
		batteries[i].xPos = (width * (1/(numCells+1) + (i/(numCells+1))) - batteries[i].xSize/2);

		// Set the total y-position of the row of cells
		batteries[i].yPos = height/2 - batteries[i].ySize/2;
	}
}

// This function pushes the given value into the given array and deletes the oldes element
function updateArray(array, value) {
	// Remove the first element and shift everything to the left ( 0: position, 1: number of elements to delete)
	array.splice(0,1);

	// Add the new value at the end of the array
	array.push(value);
}

// This function draws the voltage plot
function drawPlot(array, xPos, yPos, xSize, ySize) {

	// Create a new plot and set its position on the screen
	var plot = new GPlot(this);

	// Set the position of the plot
	plot.setPos(xPos, yPos);

	// Set the input array of the plot
	plot.setPoints(array);

	// Hide x-axis tick labels
	plot.getXAxis().setDrawTickLabels(false);

	// Hide the x-axis ticks
	plot.getXAxis().setTickLength(0);

	// Disable rotation of y-axis labels
	plot.getYAxis().rotateTickLabels = false;
	
	// Set the dimensions of the plot
	plot.setDim(xSize, ySize);
	//plot.setOuterDim(xSize, ySize);

	// Set the print margins around the plot
	plot.setMar(0, 30, 40, 0);

	// Disable printing of measured points
	plot.setPointSize(0);

	// Set x-axis label
	plot.getXAxis().setAxisLabelText("Cell Voltage [V]");

	// Set offset of x-axis label
	plot.getXAxis().getAxisLabel().setOffset(15);
	//plot.getYAxis().setAxisLabelText("y axis");
	//plot.setTitleText("A very simple example");

	// Draw the plot
	plot.defaultDraw();

	//noLoop();
}

// This function checks if the voltage values are up to date
function checkFreshness() {
	var now = new Date();
	for(var i = 0; i < numCells; i++) {
		if (now.getTime() - batteries[i].voltageTimestamp.getTime() > voltageTimeout) {
			//console.log("Cell " + batteries[i].ID + " not up to date");
			batteries[i].voltageUptodate = false;
		}
		else {
			batteries[i].voltageUptodate = true;
		}
	}
}

// A function to round floating numbers
Number.prototype.round = function(places) {
  return +(Math.round(this + "e+" + places)  + "e-" + places);
}
