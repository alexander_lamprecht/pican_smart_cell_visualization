var num_cells = 0

var balancing = false

var monitoring = false

var blocked_cells = []
var bal_arrs = []

var soc_charts = []
var voltage_charts = []
var current_charts = []
var temp_charts = []

var info_active = -1

var cell_data = []

CellData = function (id) {
  this.id = id
  this.time = []
  this.V = []
  this.soc = []
  this.I = []
  this.T = []
  this.length = 300

  this.soc_chart = []
  this.voltage_chart = []
  this.current_chart = []
  this.temp_chart = []

  this.init = false

  this.init_charts = function () {
    this.soc_chart = new CellInfoChart(this.id, 'soc', [], [])
    this.voltage_chart = new CellInfoChart(this.id, 'voltage', [], [])
    this.current_chart = new CellInfoChart(this.id, 'current', [], [])
    this.temp_chart = new CellInfoChart(this.id, 'temperature', [], [])
  }

  this.update_charts = function () {
    this.soc_chart.updateChart(this.time, this.soc)
    this.voltage_chart.updateChart(this.time, this.V)
    this.current_chart.updateChart(this.time, this.I)
    this.temp_chart.updateChart(this.time, this.T)
  }

  this.get_val = function (type) {
    if (!monitoring) {
      return 0
    }
    switch (type) {
      case 'time':
        return (this.time[this.time.length - 1])
      case 'soc':
        return (this.soc[this.soc.length - 1])
      case 'voltage':
        return (this.V[this.V.length - 1])
      case 'current':
        return (this.I[this.I.length - 1])
      case 'temperature':
        return (this.T[this.T.length - 1])
      default:
        return (0)
    }
  }

  this.update = function (type, val) {
    if (!this.init) {
      this.time = []
      this.V = []
      this.soc = []
      this.I = []
      this.T = []
      this.init = true
    }
    switch (type) {
      case 'time':
        if (this.time.length > this.length) {
          this.time.shift()
        }
        this.time.push(val)
        break
      case 'soc':
        if (this.soc.length > this.length) {
          this.soc.shift()
        }
        this.soc.push(val)
        break
      case 'voltage':
        if (this.V.length > this.length) {
          this.V.shift()
        }
        this.V.push(val)
        break
      case 'current':
        if (this.I.length > this.length) {
          this.I.shift()
        }
        this.I.push(val)
        break
      case 'temperature':
        if (this.T.length > this.length) {
          this.T.shift()
        }
        this.T.push(val)
        break
      default:
        break
    }
  }
}
