var socket
var ipAdd = location.host

if (ipAdd.includes('127.0.0.1')) { // check if locally run or externally
  ipAdd = '10.25.191.203'
}

// console.log(ip_add);
var port = '80'
var socket = io.connect(ipAdd + ':' + port)

var session_id

socket.on('connect', function () {
  session_id = socket.id //
  console.log('Websocket connected!')
  console.log('session_id: ', session_id)
  bindSockets()
})





function reqNumCells () {
  socket.emit('num_of_cells_req')
}

function getVal (id, num_of_val, time) { // time(in seconds)
  socket.emit('log_req', [id, num_of_val, time])
}

// To enable balancing
function enableBal () {
  socket.emit('enable_bal_req')
	console.log('enable balancing request sent!')
}

// To disable balancing
function disableBal () {
  for (var i = 0; i < 5; i++) {
    socket.emit('disable_bal_req')
		console.log('disable balancing request sent!')
  }
}

function bindSockets () {
  initListener = socket.once('num_of_cells_res', function (data) {
    num_cells = data
		for (var i = 0; i < num_cells; i++) {
      cell_data[i] = new CellData(i)
    }
    initVisuals(num_cells)
    })

    blockingListener = socket.on('bal_start', function (data) {
    // console.log('bal_start: ',data);
    // addBalBlock(id);
    addBalArr(data[0], data[1])
  })

    blockingListener = socket.on('bal_stop', function (data) {
    // console.log('bal_stop: ',data);
    // removeBalBlock(id);
    removeBalArr(data[0], data[1])
  })
}

function removeSocketListener (handler_str) {
  socket.removeAllListeners(handler_str)
}
