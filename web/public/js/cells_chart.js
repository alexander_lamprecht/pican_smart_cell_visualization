
function CellChart () {
  this.canvas = $('.chart-container')[0]
  this.init = false
  this.type = ''
  this.ymin = 0
  this.ymax = 100
  this.vals = []
  this.labels = []
  this.datasets = []
  this.color = [
    'rgb(0, 117, 189)',
    'rgb(8, 169, 134)',
    'rgb(150, 51, 230)',
    'rgb(240, 200, 12)',
    'rgb(231, 115, 38)'
  ]

  this.dataset = {
    data: [],
    borderColor: 'gray',
    backgroundColor: 'gray',
    borderWidth: 4,
    fill: false,
    radius: 0
  }

  // this.datasets = [this.dataset,this.dataset,this.dataset,this.dataset,this.dataset]

  this.data = {
    labels: this.labels,
    datasets: this.datasets
  }

  this.options = {
    title: {
      display: true,
      text: this.type,
      fontSize: 30,
      fontFamily: "'Arial', 'Helvetica', sans-serif",
      fontColor: 'black',
      fontStyle: 'normal'
    },
    responsive: true,
    animation: false,
    tooltips: {
      mode: 'index',
      intersect: false
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    legend: {
      labels: {
        fontSize: 22,
        fontFamily: "'Arial', 'Helvetica', sans-serif",
        fontColor: 'black'
      }
    },
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          unit: 'minute'
        },
        distribution: 'linear',
        color: 'gray',
        gridLines: {
          // display: false,
          lineWidth: 1,
          color: 'gray'
        },
        ticks: {
          display: true,
          fontFamily: "'Arial', 'Helvetica', sans-serif",
          fontColor: 'black',
          fontSize: 18
        }
      }],
      yAxes: [{
        color: 'gray',
        gridLines: {
          // display: false,
          lineWidth: 1,
          color: 'gray'
        },
        ticks: {
          display: true,
          fontFamily: "'Arial', 'Helvetica', sans-serif",
          fontColor: 'black',
          fontSize: 18
          // min: this.ymin,
          // max: this.ymax
        }
      }]
    }
  }

  this.chart = new Chart(this.canvas, {
    type: 'line',
    data: this.data,
    options: this.options
  })

  this.is_init = function () {
    return this.init
  }

  this.init_chart = function (type, tStamp, data) {
    this.type = type

    switch (this.type) {
      case 'SOC':
        [this.ymin, this.ymax] = [0, 100]
        this.chart.options.title.text = 'SoC'
        break
      case 'V':
        [this.ymin, this.ymax] = [2.8, 4]
        this.chart.options.title.text = 'Voltage'
        break
      case 'I':
        [this.ymin, this.ymax] = [-2, 2]
        this.chart.options.scales.yAxes[0].ticks.min = this.ymin
        this.chart.options.scales.yAxes[0].ticks.max = this.ymax
        this.chart.options.title.text = 'Current'
        break
      case 'T':
        [this.ymin, this.ymax] = [-10, 50]
        this.chart.options.title.text = 'Temperature'
        break
      default:
        break
    }
    // this.chart.options.scales.yAxes[0].ticks.min = this.ymin
    // this.chart.options.scales.yAxes[0].ticks.max = this.ymax

    this.chart.data.labels = tStamp

    for (var cellID of Object.keys(data)) {
      this.chart.data.datasets[cellID] = {
        label: 'Cell ' + cellID,
        lineTension: 0.1,
        data: data[cellID][type],
        borderColor: this.color[cellID],
        backgroundColor: this.color[cellID],
        borderWidth: 3,
        fill: false
      }
    }
    this.chart.update()
    this.init = true
  }

  this.add_data = function (tStamp, data) {
    if (this.init) {
      var type_ = ''
      switch (this.type) {
        case 'SOC':
          type_ = 'soc'
          break
        case 'V':
          type_ = 'voltage'
          break
        case 'I':
          type_ = 'current'
          break
        case 'T':
          type_ = 'temperature'
          break
        default:
          break
      }

      this.chart.data.labels.push(tStamp)
      var i = 0
      this.chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data[i][type_])
        i++
      })
      this.chart.update()

      if (this.data.labels.length > 500) {
        this.chart.data.labels.splice(0, 1)
        this.chart.data.datasets.forEach((dataset) => {
          dataset.data.splice(0, 1)
        })
        this.chart.update()
      }
    }
  }

  this.clear_data = function () {
    this.chart.data.labels = []
    this.chart.data.datasets.forEach((dataset) => {
      dataset.data = []
    })
    this.chart.update()
  }
}