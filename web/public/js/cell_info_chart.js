
function CellInfoChart(id,type,inp_labels,inp_data){

    this.id = id
    this.type = type
    
    switch(type){
        case 'soc':
            [this.ymin, this.ymax] = [0, 100]
            break
        case 'voltage':
            [this.ymin, this.ymax] = [2.8, 4]
            break
        case 'current':
            [this.ymin, this.ymax] = [-10, 10]
            break
        case 'temperature':
            [this.ymin, this.ymax] = [-10, 50]  
            break
        default:
            break
    }
   
    // var element = '#'+type+'-chart-'+id
    // var chart_container = document.getElementById('#'+type+'-chart-'+id);
    this.chart_container = $('#'+this.type+'-chart-'+this.id)[0];
    // console.log(element,chart_container);

    this.data = {
        labels: inp_labels,
        datasets: [{
            data: inp_data,
            borderColor: "white",
            borderWidth: 4,
            fill: false,
            radius: 0
        }]
    };


    this.options = {
        animation: false,
        legend: {
            display: false
         },
         tooltips: {
            enabled: false
         },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    unit: 'minute'
                },
                distribution: 'linear',
                color: "white",
                gridLines: {
                    display: false,
                    lineWidth: 3,
                    color: "white",
                },
                ticks: {
                    display: true,
                    fontFamily: "'Arial', 'Helvetica', sans-serif",
                    fontColor: "white",
                    fontSize: 24,
                  }
            }],
            yAxes: [{
                color: "white",
                gridLines: {
                    display: false,
                    lineWidth: 3,
                    color: "white",
                },   
                ticks: {
                    display: true,
                    fontFamily: "'Arial', 'Helvetica', sans-serif",
                    fontColor: "white",
                    fontSize: 24,
                    min: this.ymin,
                    max: this.ymax
                  }
            }]
        }  
    }

    
    this.paintChart = function(canvas,data,options){
        this.chart = new Chart(canvas, {
            type: 'line',
            data: data,
            options: options
        })
    }



    this.updateChart = function(labels,data){
        if (this.chart) {
            this.chart.clear();
            this.chart.destroy();
            delete this.chart;
        }
        this.data = {
            labels: labels,
            datasets: [{
                data: data,
                borderColor: "white",
                borderWidth: 4,
                fill: false,
                radius: 0
            }]
        };

        this.paintChart(this.chart_container,this.data,this.options)
    }

    this.paintChart(this.chart_container, this.data, this.options)

}