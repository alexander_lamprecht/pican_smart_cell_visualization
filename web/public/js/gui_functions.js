// function toggleInfoPane(id){

// }
function addHandlers () {
  addCellHandlers()
  addOverlayHandler()
  addToggleButtonhandlers()
}

function addCellHandlers () {
  for (i = 0; i < num_cells; i++) {
    addCellHandler(i)
  }
}

function removeHtmlByClass (cls) {
  $('.' + cls).remove()
}

function addCellHandler (id) {
  var cell = $('#cell-' + id)
  var cellInfo = $('#cellinfo-' + id)
  cell.click(function (e) {
    // $( ".overlay-view" ).slideToggle( "slow");
    $('.overlay-view').addClass('active')
    cellInfo.addClass('active')
    info_active = id
  })
}

function addOverlayHandler () {
  var overlay = $('.overlay')
  overlay.click(function (e) {
    $('.overlay-view').removeClass('active')
    $('.info-container').removeClass('active')
    info_active = -1
  })
}

function addToggleButtonhandlers () {
  var buttons = $('.toggle-button')
    buttons.click(function (e) {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active')
        } else{
      $(this).addClass('active')        
        }
  })
}

function activateButton (id) {
  if (!$('#' + id).hasClass('active')) {
    $('#' + id).addClass('active')
    // console.log(id+' activated')
  }
}
