// function getRnd(min, max) {
//     return Math.random() * (max - min) + min;
// }

// function updateCellValHTMLs(){  //time(in seconds)
//     // for(i=0;i<num_cells;i++){
//     for(i=0;i<num_cells;i++){
//         soc_arr = [];
//         v_arr = [];
//         socket.emit("log_req", [i, 1, 0]);
//     }
// }

function initCellVals () {
  for (var i = 0; i < num_cells; i++) {
    updateCellValHTML(i)
    updateCellInfoHTML(i)
  }
}

function toggleListening () {
  if (socket._callbacks['$current_val_br'] == undefined) {
    // Handler not present, install now
    socket.on('current_val_br', function (data) {
      cell_val = JSON.parse(data)['cell_data']
      for (var cell in cell_val) {
        updateCellArr(cell, cell_val[cell]['soc'], cell_val[cell]['voltage'], cell_val[cell]['temperature'], cell_val[cell]['current'])
        // updateCellValHTML(cell,cell_val[cell]['soc'],cell_val[cell]['voltage'])
        updateCellValHTML(cell)
        updateCellInfoHTML(cell)
        if (monitoring && info_active == cell) {
          cell_data[cell].update_charts()
        }
      }
      // if (info_active>=0 && monitoring){ // this is too slow; slows down the entire raspberry. this kind of request cannot be sent periodically!!!!!
      //     getVal(info_active,100,1)
      // }
    })
    socket.on('log_res', function (data) {
      data_arr = JSON.parse(data)
      console.log(data_arr)
      updateCellInfoChart(data_arr)
    })
    // console.log('current_val_br handler installed')
    monitoring = true
    displayBlocks()
    displayArrs()
  }else {
    removeSocketListener('current_val_br')
    removeSocketListener('log_res')
    monitoring = false
    initCellVals()
    displayBlocks()
    displayArrs()
  }
}

function toggleBalState () {
  balancing = !balancing
}

function toggleBalancing () {
  if (balancing) {
    disableBal()
  }else {
    enableBal()
  }
  toggleBalState()
}

function disableBalancing () {
  disableBal()
  balancing = false
}

function updateCellArr (id, soc, voltage, temp, curr) {
  cell_data[id].update('time', new Date())
  cell_data[id].update('soc', soc)
  cell_data[id].update('voltage', voltage)
  cell_data[id].update('current', curr)
  cell_data[id].update('temperature', temp - 273.15)
}

function updateCellValHTML (id) {
  // var cell = document.getElementById("cell-"+id);
  $('#cell-' + id + ' .socval').html('SoC: ' + cell_data[id].get_val('soc').toFixed(0) + '%')
  $('#cell-' + id + ' .voltageval').html('Voltage: ' + cell_data[id].get_val('voltage').toFixed(3) + 'V')
  updateSocBars(id, cell_data[id].get_val('soc'))
}

function updateCellInfoHTML (id) {
  // var cell = document.getElementById("cell-"+id);
  $('#cellinfo-' + id + ' .soc-chart-container .value span').html(cell_data[id].get_val('soc').toFixed(0) + '%')
  $('#cellinfo-' + id + ' .voltage-chart-container .value span').html(cell_data[id].get_val('voltage').toFixed(3) + 'V')
  $('#cellinfo-' + id + ' .current-chart-container .value span').html(cell_data[id].get_val('current').toFixed(1) + 'A')
  $('#cellinfo-' + id + ' .temperature-chart-container .value span').html(cell_data[id].get_val('temperature').toFixed(0) + '&degC')
}

function updateSocBars (id, soc) {
  $('#cell-' + id + ' .soc').css('max-height', soc + '%')
  $('#cell-' + id + ' .green').css('opacity', Math.min(Math.max(0, (soc - 50) * 2) / 100, 1))
  $('#cell-' + id + ' .yellow').css('opacity', Math.min(Math.max(0, (soc) * 2) / 100, 1))
}

function updateNoCells () {
  socket.on('num_of_cells_res', function (data) {
    num_cells = data
  })
}

function displayBlocks () {
  removeHtmlByClass('block')
  if (monitoring) {
    for (i = 0; i < blocked_cells.length; i++) {
      if (blocked_cells[i][0] > 0) {
        removeHtmlByClass('block' + (blocked_cells[i][0] - 1))
        $('.bal-blocks').append('<div class="block block' + (blocked_cells[i][0] - 1) + '"></div>')
      }
      if (blocked_cells[i][1] < num_cells - 1) {
        removeHtmlByClass('block' + (blocked_cells[i][1] + 1))
        $('.bal-blocks').append('<div class="block block' + (blocked_cells[i][1] + 1) + '"></div>')
      }
    }
  }
}

function addBalBlock (left, right) {
  balancing = true
  activateButton('bal-button')
  if (!blocked_cells.includes([left, right])) {
    blocked_cells.push([left, right])
    // console.log('block:',[left,right])
  }
  blocked_cells = blocked_cells.sort()
  // removeHtmlByClass("block"+id);
  // $('.bal-blocks').append('<div class="block block'+id+'"></div>');
  displayBlocks()
}

function removeBalBlock (id) {
  blocked_cells.splice(blocked_cells.indexOf(id), 1)
  // removeHtmlByClass("block"+id);
  displayBlocks()
}

function displayArrs () {
  removeHtmlByClass('arrow')
  if (monitoring) {
    for (i = 0; i < bal_arrs.length; i++) {
      // var dir = 'right'
      if (bal_arrs[i][1] > bal_arrs[i][2]) { // origin greater target
        var dir = 'left'
        var ori = bal_arrs[i][2]
        var tar = bal_arrs[i][1]
      } else{
        var dir = 'right'
        var ori = bal_arrs[i][1]
        var tar = bal_arrs[i][2]
      }
      var app_str = '<div class="arrow ' + dir + ' start' + ori + ' end' + tar + '"></div>'
      $('.bal-arrows').append(app_str)
    }
  }
}

function addBalArr (origin, target) {
  balancing = true
    activateButton('bal-button')
  var id = origin + '-' + target
  bal_arrs.push([id, origin, target])
  bal_arrs = bal_arrs.sort()
  displayArrs()
  addBalBlock(Math.min(origin, target), Math.max(origin, target))
}

function removeBalArr (origin, target) {
  var id = origin + '-' + target
  bal_arrs.splice(blocked_cells.indexOf([id, origin, target]), 1)
  displayArrs()
  removeBalBlock(Math.min(origin, target) - 1)
  removeBalBlock(Math.max(origin, target) + 1)
}

function initVisuals (numCells) {
  displayBlocks()
  displayArrs()
  removeHtmlByClass('cell')
  for (var i = 0; i < numCells; i++) {
    // create cells for the pack display
    $('.cell-container').append('<a id="cell-' + i + '" class="cell" href=#></a>')

    $('#cell-' + i).append('<div class="outline"></div>')
    $('#cell-' + i + ' .outline').append('<div class="soc red"></div><div class="soc yellow"></div><div class="soc green"></div><div class="info">')
    $('#cell-' + i + ' .info').append('<p class="socval"></p><p class="voltageval"></p>')

    updateCellValHTML(i)

    // create the info screens
    $('.info-bg').append('<div class="info-container" id="cellinfo-' + i + '">')

    info_obj = $('#cellinfo-' + i)
    // add headline
    info_obj.append('<h1>Cell ' + i + '</h1>')
    // add SOC container
    info_obj.append('<div class="chart-container soc-chart-container">')
    info_obj.find('.soc-chart-container').append('<div class="value"><h2>SOC</h2><span>0%</span></div>')
    info_obj.find('.soc-chart-container').append('<canvas class="cell-chart" id="soc-chart-' + i + '"></canvas>')
    // add Voltage container
    info_obj.append('<div class="chart-container voltage-chart-container">')
    info_obj.find('.voltage-chart-container').append('<div class="value"><h2>Voltage</h2><span>0.00V</span></div>')
    info_obj.find('.voltage-chart-container').append('<canvas class="cell-chart" id="voltage-chart-' + i + '"></canvas>')
    // add current container
    info_obj.append('<div class="chart-container current-chart-container">')
    info_obj.find('.current-chart-container').append('<div class="value"><h2>Current</h2><span>0.0A</span></div>')
    info_obj.find('.current-chart-container').append('<canvas class="cell-chart" id="current-chart-' + i + '"></canvas>')
    // add temperature container
    info_obj.append('<div class="chart-container temperature-chart-container">')
    info_obj.find('.temperature-chart-container').append('<div class="value"><h2>Temperature</h2><span>0&deg C</span></div>')
    info_obj.find('.temperature-chart-container').append('<canvas class="cell-chart" id="temperature-chart-' + i + '"></canvas>')
    // add balancing info
    info_obj.append('<div class="balancing-info"></div>')

    updateCellInfoHTML(i)

    cell_data[i].init_charts()
  }
  addHandlers()
  console.log('Pack initialization successful!')
}
