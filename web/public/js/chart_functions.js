// -------------------------------------------------------------------
// Filename:        chart_functions.js
// Project name:    Internet of Things of smart decentralised cells
// Author:          Alexander Lamprecht
// Comments:        This file contains functions that provides the functionality for the chart view
// -------------------------------------------------------------------

var chart = []

var dataArr = []

var chartType = 'SOC' // I,V
var chartTime = 1800
var chartRes = 60

var msgCnt = 0

var req_timeout = 10000

var monitoring = true

var resPending = false

var balancing = false

/** This is a description of the foo function. */
function bindSockets () {
  socket.on('log_res', function (data) {
    dataArr = JSON.parse(data)
    chart = new CellChart()
    chart.init_chart(chartType, dataArr['t_arr'], dataArr['cells'])
    resPending = false
  })
  socket.on('current_val_br', function (data) {
    var cellData = JSON.parse(data)['cell_data']
    if (monitoring && chart.is_init()) {
      if (msgCnt >= chartRes) {
        chart.add_data(new Date(), cellData)
        msgCnt = 0
      } else {
        msgCnt++
      }
    }
  })
  socket.on('bal_start', function () {
    balancing = true
    $('#bal-btn').addClass('toggle-active')
  })
}

/**
 * This is a description of the foo function.
 * @param {string} type - selector for the chart content to display
 *  */
function setChartType (type) {
  if (chartType !== type) {
    chartType = type
    chart.init = false
    // init_chart()
    $('.toggle-button').removeClass('active')
    $('#' + type + '-btn').addClass('active')
    chartRes = 60
    if (!resPending) {
      getVal(-1, chartTime, chartRes)
    }
    chartRes = 5
    resPending = true
  }
}

// setInterval(function() {
//     getVal(-1,chart_time,chart_res);
//     console.log(new Date)
// }, req_timeout) // wait for a while until next request (req_timeout)

/** This is a description of the foo function. */
function add_click_listeners () {
  $('#bal-btn').click(function (e) {
    if ($(this).hasClass('toggle-active')) {
      $(this).removeClass('toggle-active')
    } else {
      $(this).addClass('toggle-active')
    }
  })
}

/** This is a description of the foo function. */
function init_chart () {
  chart = new CellChart()
  if (!resPending) {
    getVal(-1, chartTime, chartRes)
  }
  resPending = true
  chartRes = 5
}

/** This is a description of the foo function.
 * @param {int} t - time of the requested interval in seconds
 * @param {int} res - resultion of the requested data
*/
function request_log (t, res) {
  if (!resPending) {
    getVal(-1, t, res)
  }
  resPending = true
}

/** This is a description of the foo function. */
function clear_chart () {
  chart.clear_data()
  chartRes = 1
}

/** This is a description of the foo function. */
function toggleBalState () {
  balancing = !balancing
}

/** This is a description of the foo function. */
function toggleBalancing () {
  if (balancing) {
    disableBal()
  } else {
    enableBal()
  }
  toggleBalState()
}

/** This is a description of the foo function. */
function disableBalancing () {
  disableBal()
  balancing = false
}
