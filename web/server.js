
// ---------------------------------------------------------------------------------
// Filename:        server.js
// Project name:    Internet of Things of smart decentralised cells
// Author:          Satyam Gaba
// Supervisors:     Alexander Lamprecht
// Comments:        Node Server file to provide a websocket and forwarding data 
// 					between python and clients
// Usage: 			$sudo node server.js
// ---------------------------------------------------------------------------------

var myPort = 80

var express = require('express')
var socket = require('socket.io')

var app = express()
app.use(express.static('public'))


var server = app.listen(myPort)
var io = socket(server)

var clients = []

io.sockets.on('connection', function(socket){

	console.log('new connection: '+ socket.id)
	clients.push(socket.id)
	
	socket.on('num_of_cells_req', forwardNumOfCellsReq);
	socket.on('num_of_cells_res', forwardNumOfCellsRes);
	socket.on('current_val_br', forwardPeriodicCellVal);
	socket.on('log_req', forwardLogReq);
	socket.on('log_res', forwardLogRes);
	socket.on('bal_state', forwardBalState);
	socket.on('cell_state_block', forwardBlockingState);
	socket.on('cell_state_unblock', forwardUnBlockingState);
	socket.on('enable_bal_req', enabBal);
	socket.on('disable_bal_req', disabBal);
	socket.on('bal_start', balStart);
	socket.on('bal_stop', balStop);
	
	function forwardNumOfCellsReq(){
		console.log("Request: num_of_cells_req from: js");
		socket.broadcast.emit('num_of_cells_req');
	}
	
	function forwardNumOfCellsRes(data){
		console.log("Request: num_of_cells_res from: python");
		console.log("Response: ", data);
		socket.broadcast.emit('num_of_cells_res', data);
	}

	function forwardPeriodicCellVal(data){
		console.log("Request: current_val_br from: python");
		// console.log("Response: ",data);
		socket.broadcast.emit('current_val_br', data);
	}

	function forwardLogReq(data){
		console.log("Request: log_req from: js");
		console.log("Request data: ",data);
		socket.broadcast.emit('log_req', {'target_id': socket.id, 'msg': data});
	}

	function forwardLogRes(data){
		console.log("Request: log_res from: python");
		// console.log("Request data: ",data);
		console.log(data['target_id'],data['msg'])
		socket.to(data['target_id']).emit('log_res',data['msg']);
	}

	function forwardBalState(data){
		console.log("Request: bal_state from: python");
		socket.broadcast.emit('bal_state',data);
	}
	
	function forwardBlockingState(data){
		console.log("Request: cell_state_block from: python");
		socket.broadcast.emit('cell_state_block',data);
	}

	function forwardUnBlockingState(data){
		console.log("Request: cell_state_unblock from: python");
		socket.broadcast.emit('cell_state_unblock',data);
	}
	
	function enabBal(){
		console.log("Request: enable_bal_req from: js");
		socket.broadcast.emit('enable_bal_req');
	}
	
	function disabBal(){
		console.log("Request: disable_bal_req from: js");
		socket.broadcast.emit('disable_bal_req');
	}

	function balStart(data){
		console.log("Request: bal_start from: python");
		socket.broadcast.emit('bal_start',data);
	}

	function balStop(data){
		console.log("Request: bal_start from: python")
		socket.broadcast.emit('bal_stop',data);
	}
})


console.log("Socket server is running on port: " + myPort);
